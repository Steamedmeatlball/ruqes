# RuqES

RuqES is a userscript, small piece of code which is installed in your browser and can alter how sites look and work, in this case [Ruqqus.com](https://ruqqus.com).

For a list of features and more details (e.g. installation), please see https://ruqqus.com/post/ldx/what-is-ruqes.

# Post rules

Only ES5 syntax is supported, so sadly no fat arrows `=>` for us (but functions like `startsWith` or `includes` are working).

For a quick start see [RECIPES](RECIPES.md). API is available [here](src/modules/post/postRulesApiTypes.ts).

# Development

## Setup

Install appropriate [Node](https://nodejs.org) version (stated in [.node-version](.node-version)). You can use [n](https://github.com/tj/n) or other Node managers.

```sh
$ npm i
```

## Watch mode

```sh
$ npm start
```

Install proxy script from: `http://localhost:8080/ruqes.proxy.user.js`.

## Build

```sh
$ npm run build
```

Output is located in `dist` directory.

# TODO
- update features in pinned post
- refactor settings so module containers are located in corresponding module classes
- remove deprecated features (probably in a version after integrated recipes)
- also show preview in post/comment edit
- support tab / some way to donate

# Ideas
- add support for uploading images to catbox.moe - https://catbox.moe/tools.php (old school form format?)
- more embeds: instagram, soundcloud
- federated embeds? adding instances by hand or some HEAD trickery? (mastodon, pleroma)
- option to tag users (tag label + color?)
- option for expando button to span post title and actions
- compatibility with Waterfox Classic? (at least `matchAll` shim/polyfill, possibly more)
- thumbnail icon for links to ruqqus?
- spoiler tag (reddit: `>!spoiler!<`, commonmark plugin: `>>!spoiler!<<`, discord: `||spoiler||`)
- syntax highlighting in code tag
- temporarily enable specific post rule for given tab
- post rules engine: option to disable short circuit?
- support for older GM_ API?
- keyboard shortcuts for next post and expando - https://ruqqus.com/post/1e8v/suggestion-keyboard-shortcuts
  - maybe for start just a select with few options? or jump straight in any key/mouse button way?
- stats in user profile - e.g. total number, score, avg. score of posts and comments (probably when you reach end of infinite scroll?)
- some repost check assistance (search by title, maybe by domain + title/url on ddg)
- destroy embeds (e.g. a YT/BC video) when closing expando button?
- customizable font size, font family
- stats of user's rep (chart)
- "my guilds"/favorites in sidebar could become an icon grid (user specifies guild names and the script replaces content with guild icons without names; customizable tags/icons?)
- counters with up/down-votes of other users
- account switching
- remember page in infinite scroll, so navigating back work more expectedly (rework infinite scroll to cache all loaded posts and reconstructs them if needed? or just cache responses and scroll down until requested page?)
- join/leave button from post on a list - should be possible get the data from `...` menu in post item. is it worth it though?
- option to hide avatars, images and emojis (replace with asciimoji?)

# License
AGPL3
