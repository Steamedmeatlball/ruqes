// ==UserScript==
// @name        RuqES - ruqqus.com
// @version     0.26.0
// @author      enefi
// @description "Ruqqus Enhancement Suite" brings infinite scroll, expando button and more. Recommended to be used with Violentmonkey. For more details see https://ruqqus.com/post/ldx/what-is-ruqes
// @supportURL  https://ruqqus.com/post/p04/bug-reports-and-other-issues
// @match       https://ruqqus.com/*
// @match       https://dev.ruqqus.com/*
// @match       https://linode.ruqqus.com/*
// @match       http://ruqqus.localhost:8000/*
// @namespace   enefi
// @grant       GM.xmlHttpRequest
// @grant       GM.info
// @grant       GM.getValue
// @grant       GM.setValue
// @require     https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js
// @require     http://localhost:8080/ruqes.user.js
// @homepageURL https://ruqqus.com/post/ldx/what-is-ruqes
// @runAt       document-start
// ==/UserScript==
