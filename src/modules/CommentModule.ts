import {prop, splitAt} from 'ramda';
import debounce from 'lodash.debounce';

import {debugLog, genTextUpDownCounters, isCommentDeletedByAuthor, parseUpDownCounts, printError} from 'common/common';
import {
  clsPrefix,
  commentBiggerFoldButtonsCls,
  commentPreviewCls,
  commentUpDownVotesAsTextProcessedCls,
  improvedTableCls,
  commentPreviewAppliedCls,
  commentCollapseByRegexProcessedCls,
  commentImageUploadProcessedCls,
} from 'common/styles';
import {RuqESModule} from 'common/RuqESModule';
import {ModuleSetupArgs} from 'common/modules';
import {Config} from 'common/config';
import {getCommentReplyToPart, getComments, getReplyToPost} from 'common/selectors';
import {renderMarkdown} from 'modules/createPost/renderMarkdown';
import {SaveModule} from './save/SaveModule';
import {ImageUploadButton} from './external/ImageUploadButton';

const genCommentId = (x: string) => `${clsPrefix}${x}`;

const processedCommentBoxMarkCls = genCommentId('processed-comment-box');

export class CommentModule extends RuqESModule {
  setupCtrlEnterToSend() {
    $('.comment-box')
      .filter((_, el) => !$(el).hasClass(processedCommentBoxMarkCls))
      .keydown(evt => {
        if (evt.key === 'Enter' && (evt.ctrlKey || evt.metaKey)) {
          debugLog('setupCtrlEnterToSend', 'CTRL+Enter detected', evt);
          const btn = $(evt.target).parent().find('a:contains(Comment)');
          if (btn.length > 1) {
            printError('setupCtrlEnterToSend', 'found multiple send buttons', btn);
          } else if (btn.length === 0) {
            printError('setupCtrlEnterToSend', 'no send button found', btn);
          } else {
            debugLog('setupCtrlEnterToSend', 'clicking on send button', btn);
            btn.click();
          }
        }
      })
      .addClass(processedCommentBoxMarkCls);
  };

  private setupBiggerFoldButtons() {
    $('body').addClass(commentBiggerFoldButtonsCls);
  }

  private setupUpDownVotesAsText() {
    getComments()
      .filter((_, rawEl) => !$(rawEl).hasClass(commentUpDownVotesAsTextProcessedCls))
      .each((_, rawEl) => {
        const el = $(rawEl);
        el.addClass(commentUpDownVotesAsTextProcessedCls);
        const pointsEl = el.find('.points');
        const [upCount, downCount] = parseUpDownCounts(pointsEl.data('originalTitle'));
        const counters = genTextUpDownCounters(upCount, downCount);
        el.find('.user-info .time-stamp').after(counters);
      })
    ;
  }

  private setupOneCommentPreview(cfg: Config, textArea: JQuery, insertPreviewBox: (_: JQuery) => void) {
    if (textArea.hasClass(commentPreviewAppliedCls)) {
      return;
    }
    textArea.addClass(commentPreviewAppliedCls);
    const previewBox = $('<div>').addClass(commentPreviewCls);
    if (cfg.post.improvedTableStyles) {
      previewBox.addClass(improvedTableCls);
    }
    const inputChangedHandler = debounce((evt: JQuery.ChangeEvent) => {
      const el = $(evt.target);
      previewBox.html(renderMarkdown(String(el.val())));
    }, 200);
    textArea.on('input', inputChangedHandler);
    insertPreviewBox(previewBox);
  }

  private setupPreview(cfg: Config) {
    const replyToPostEl = getReplyToPost();
    if (replyToPostEl?.length !== 1) {
      debugLog('CommentModule', 'setupPreview', 'failed to locate reply to post element', replyToPostEl);
    } else {
      this.setupOneCommentPreview(cfg, $('textarea[name="body"]', replyToPostEl), previewBox => replyToPostEl.append(previewBox));
    }
    if (SaveModule.isOnSavedPage()) { return; }
    getComments().each((_, rawEl) => {
      const el = $(rawEl);
      if (isCommentDeletedByAuthor(el)) { return; }
      const replyEl = getCommentReplyToPart(el);
      if (!replyEl) {
        printError('CommentModule', 'setupPreview', 'failed to locate reply element', {el, replyEl});
        return;
      }
      this.setupOneCommentPreview(cfg, $('textarea[name="body"]', replyEl), previewBox => replyEl.find('form').after(previewBox));
    });
  }

  private setupAutoCollapsingOfCommentsByRegex(cfg: Config) {
    debugLog('setupAutoCollapsingOfCommentsByRegex', cfg.comment);
    const strRegex = cfg.comment.autoCollapseCommentsRegex;
    if (!strRegex) { return; }
    let regex: RegExp;
    try {
      regex = new RegExp(strRegex, 'isu');
    } catch (e) {
      printError('setupAutoCollapsingOfCommentsByRegex', 'invalid regex', e);
      return;
    }
    getComments().filter((_, rawEl) => !$(rawEl).hasClass(commentCollapseByRegexProcessedCls)).each((_, rawEl) => {
      const el = $(rawEl);
      el.addClass(commentCollapseByRegexProcessedCls);
      const text = el.find('.comment-text').text();
      if (regex.test(text)) {
        el.parent().parent().addClass('collapsed');
      }
    });
  }

  private setupImageUpload(cfg: Config) {
    debugLog('setupImageUpload', cfg.comment, cfg.external);
    const processComment = (_, rawEl) => {
      const el = $(rawEl);
      if ($(rawEl).hasClass(commentImageUploadProcessedCls)) { return; }
      el.addClass(commentImageUploadProcessedCls);
      const replyEl = getCommentReplyToPart(el) ?? getReplyToPost();
      if (!replyEl) {
        printError('CommentModule', 'setupImageUpload', 'failed to locate reply element', {el, replyEl});
        return;
      }
      const lastActionButton = replyEl.find('.comment-format label').last();
      const onSuccess = url => {
        debugLog('setupImageUpload', 'onSuccess', url, cfg);
        const textEl = replyEl.find('textarea[name=body]');
        if (!textEl.length) {
          printError('setupImageUpload', 'onSuccess', 'failed to get textEl');
          return;
        }
        const insertPos = textEl.prop('selectionStart');
        const oldText = String(textEl.val());
        const [bef, aft] = splitAt(insertPos, oldText);
        const imgText = `![](${url})`;
        const newText = bef + imgText + aft;
        debugLog({insertPos, oldText, bef, aft, imgText, newText});
        textEl.val(newText).trigger('change').trigger('input');
      };
      const btnEl = ImageUploadButton.genUploadButton('COMMENT', cfg, onSuccess);
      lastActionButton.after(btnEl);
    };
    getComments().each(processComment);
    getReplyToPost()?.each(processComment);
  }

  async setup(args: ModuleSetupArgs, cfg: Config) {
    const commentCfg = cfg.comment;
    if (!prop('silent', args)) {
      debugLog('setupComment', commentCfg);
    }
    if (!this.firstSetupRunFinished) {
      if (commentCfg.ctrlEnterToSend) {
        setInterval(this.setupCtrlEnterToSend, 1000);
      }
      if (commentCfg.biggerFoldButtons) {
        this.setupBiggerFoldButtons();
      }
    }
    if (commentCfg.upDownVotesAsText) {
      this.setupUpDownVotesAsText();
    }
    if (commentCfg.preview) {
      this.setupPreview(cfg);
    }
    if (commentCfg.hideTipButton) {
      getComments().find('[data-target="#tipModal"]').parent().css('display', 'none');
    }
    this.setupAutoCollapsingOfCommentsByRegex(cfg);
    if (commentCfg.imageUpload && cfg.external.imgbbKey) {
      this.setupImageUpload(cfg);
    }
  }

  async onContentChange(args: ModuleSetupArgs, cfg: Config) {
    await this.setup(args, cfg);
  }
}
