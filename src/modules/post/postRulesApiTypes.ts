export interface PostRulePostDescriptor {
  title: string;
  author: string;
  guild: string;
  id: string;
  nsfw: boolean;
  score: number;
  vote: 'up' | null | 'down',
  url: string;
  link: null | string;
  dateRelative: string;
  dateRaw: string;
  date: string;
  pinned: boolean;
}

export interface LocationDescriptor {
  href: string;
  path: string;
  search: string;
}

export type PostRuleAction = {
  tag: 'semiHide'
} | {
  tag: 'hide'
} | {
  tag: 'setBackground',
  color?: string
} | {
  tag: 'blurThumbnail',
};

export interface PostRuleAPI {
  semiHideAction: () => PostRuleAction;
  hideAction: () => PostRuleAction;
  setBackgroundAction: (color?: string) => PostRuleAction;
  blurThumbnailAction: () => PostRuleAction;
}
