import {prop, append, filter, pipe, reverse, map, isEmpty} from 'ramda';
import moment from 'moment';

import {RuqESModule} from 'common/RuqESModule';
import {handleModulesAfterContentChange, ModuleSetupArgs} from 'common/modules';
import {Config, PostSave, readConfig, writeConfig, CommentSave} from 'common/config';
import {
  createMainMenuButtonForDesktop, createMainMenuButtonForMobile,
  debugLog,
  extractPostInfo,
  genJsAnchor,
  genNavigateToRuqESUrl,
  genRuqESUrl,
  isOnRuqESUrl,
  PostInfo,
  $i,
  createRuqesMark,
  $c,
  extractCommentInfo,
  CommentInfo,
  setClass,
} from 'common/common';
import {getComments, getPostActionsListOnDesktop, getPostActionsListOnMobile, getPosts, getCommentActionsList} from 'common/selectors';
import {
  nonAggressiveCls,
  savePostProcessedCls,
  saveEmptySavesButtonPlaceholderCls,
  saveCommentProcessedCls,
  saveTabButtonPostsCls,
  saveTabButtonCommentsCls,
  saveCommentsContainerCls,
  savePostsContainerCls,
} from 'common/styles';
import {renderSavedPost} from './renderSavedPost';
import {renderSavedComment} from './renderSavedComment';

const savePostText = 'Save';
const savedPostText = 'Saved';
const saveIconClasses = 'fas fa-save';
const savedIconClasses = 'fal fa-save';

export class SaveModule extends RuqESModule {
  private async onSavePostButtonClick(el: JQuery, info: PostInfo) {
    debugLog('onSavePostButtonClick', {el, info});
    const cfg = await readConfig();
    let newCfg: Config;
    const updateSavedPostsInConfig = (cfg: Config) => (over: (_: PostSave[]) => PostSave[]): Config => ({
      ...cfg,
      save: {
        ...cfg.save,
        posts: over(cfg.save.posts),
      },
    });
    if (cfg.save.posts.find(x => x.id === info.id)) {
      newCfg = updateSavedPostsInConfig(cfg)(filter((x: PostSave) => x.id !== info.id));
      SaveModule.setSaveButtonState(el, false);
    } else {
      newCfg = updateSavedPostsInConfig(cfg)(append(SaveModule.convertPostInfoToPostSave(info)));
      SaveModule.setSaveButtonState(el, true);
    }
    await writeConfig(newCfg);
  }

  private async onSaveCommentButtonClick(el: JQuery, info: CommentSave) {
    debugLog('onSaveCommentButtonClick', {el, info});
    const cfg = await readConfig();
    let newCfg: Config;
    const updateSavedCommentsInConfig = (cfg: Config) => (over: (_: CommentSave[]) => CommentSave[]): Config => ({
      ...cfg,
      save: {
        ...cfg.save,
        comments: over(cfg.save.comments),
      },
    });
    if (cfg.save.comments.find(x => x.id === info.id)) {
      newCfg = updateSavedCommentsInConfig(cfg)(filter((x: CommentSave) => x.id !== info.id));
      SaveModule.setSaveButtonState(el, false);
    } else {
      newCfg = updateSavedCommentsInConfig(cfg)(append(info));
      SaveModule.setSaveButtonState(el, true);
    }
    await writeConfig(newCfg);
  }

  private static convertPostInfoToPostSave(info: PostInfo): PostSave {
    return {
      title: info.title,
      author: info.author,
      guild: info.guild,
      id: info.id,
      nsfw: info.nsfw,
      url: info.url,
      link: info.link,
      thumbnail: info.thumbnail,
      date: info.date,
      dateRelative: info.dateRelative,
      dateRaw: info.dateRaw,
      previewModal: info.previewModal,
      savedOn: moment().toISOString(),
    };
  }

  private static convertCommentInfoToCommentSave(info: CommentInfo): CommentSave | null {
    if (info.id === null || info.author === null || info.link === null || info.text === null) {
      return null;
    }
    return {
      id: info.id,
      author: info.author,
      link: info.link,
      text: info.text,
      savedOn: moment().toISOString(),
    };
  }

  private static setSaveButtonState(el: JQuery, alreadySaved: boolean) {
    el.text(alreadySaved ? savedPostText : savePostText);
    const icon = $('<i>').addClass('mr-2').addClass(alreadySaved ? savedIconClasses : saveIconClasses);
    el.prepend(icon);
  }

  private setupSavePostActions(cfg: Config) {
    getPosts()
      .filter((_, rawEl) => !$(rawEl).hasClass(savePostProcessedCls))
      .each((_, rawEl) => {
        const el = $(rawEl);
        el.addClass(savePostProcessedCls);
        const info = extractPostInfo(el);
        const alreadySaved = cfg.save.posts.find(x => x.id === info.id) !== undefined;
        const btn = genJsAnchor();
        btn.click((evt: JQuery.ClickEvent) => { this.onSavePostButtonClick($(evt.target), info); });
        SaveModule.setSaveButtonState(btn, alreadySaved);

        getPostActionsListOnDesktop(el).append($('<li>').addClass('list-inline-item').html(btn));

        const mobilePostActions = getPostActionsListOnMobile(el);
        const shareActionOnMobile = mobilePostActions.find('i.fa-link');
        const mobileListItem = $('<li>').addClass('list-inline-item').html(btn.clone(true));
        if (shareActionOnMobile.length) {
          shareActionOnMobile.parent().parent().after(mobileListItem);
        } else {
          mobilePostActions.append(mobileListItem);
        }
      })
    ;
  }

  private setupSaveCommentActions(cfg: Config) {
    getComments()
      .filter((_, rawEl) => !$(rawEl).hasClass(saveCommentProcessedCls))
      .each((_, rawEl) => {
        const el = $(rawEl);
        el.addClass(saveCommentProcessedCls);
        const info = extractCommentInfo(el);
        const alreadySaved = cfg.save.comments.find(x => x.id === info.id) !== undefined;
        const btn = genJsAnchor();
        const saveData = SaveModule.convertCommentInfoToCommentSave(info);
        if (saveData === null) { return; }
        btn.click((evt: JQuery.ClickEvent) => { this.onSaveCommentButtonClick($(evt.target), saveData); });
        SaveModule.setSaveButtonState(btn, alreadySaved);

        const actionsList = getCommentActionsList(el);

        const actionDesktopEl = $('<li>').addClass('list-inline-item d-none d-md-inline-block').html(btn);
        const actionDotsDesktop = actionsList.find('li.d-none .fa-ellipsis-h');
        if (actionDotsDesktop.length) {
          actionDotsDesktop.parent().parent().parent().before(actionDesktopEl);
        } else {
          actionsList.append(actionDesktopEl);
        }

        const actionMobileEl = $('<li>').addClass('list-inline-item d-inline-block d-md-none').html(btn.clone(true));
        const actionDotsOnMobile = actionsList.find('li.d-md-none .fa-ellipsis-h');
        if (actionDotsOnMobile.length) {
          actionDotsOnMobile.parent().parent().before(actionMobileEl);
        } else {
          actionsList.append(actionMobileEl);
        }
      })
    ;
  }

  private setupMainMenuButton() {
    const text = 'Saved';
    const iconClasses = 'fas fa-save';
    const path = 'saved';
    const desktopButton = createMainMenuButtonForDesktop(text, genRuqESUrl(path), genNavigateToRuqESUrl(path), iconClasses);
    const mobileButton = createMainMenuButtonForMobile(text, genRuqESUrl(path), genNavigateToRuqESUrl(path), iconClasses);
    $('#navbar #navbarResponsive > ul > li:last-child a > i.fa-inbox').parent().after(desktopButton);
    $('#navbarResponsive > ul:last-child a > i.fa-envelope').parent().after(mobileButton);
  }

  private genEmptySaves() {
    const btn = genJsAnchor();
    SaveModule.setSaveButtonState(btn, false);

    const html = `
<span class="fa-stack fa-2x text-muted mb-4">
  <i class="fas fa-square text-gray-500 opacity-25 fa-stack-2x"></i>
  <i class="fas text-gray-500 fa-ghost fa-stack-1x text-lg"></i>
</span>
<h2 class="h5">Save your first piece of content!</h2>
<p class="text-muted">Don't know how?<br>Just click on "<span class="${saveEmptySavesButtonPlaceholderCls}"></span>" on a post or a comment.</p>
    `;
    const el = $('<div>').html(html).addClass('text-center mt-4');
    $c(saveEmptySavesButtonPlaceholderCls, el).html(btn);
    return el;
  }

  private async setupSavedPage(cfg: Config) {
    const page = $('<div>').addClass('mt-2');
    const caption = $('<div>')
      .addClass('font-weight-bold text-muted')
      .addClass('card d-flex justify-content-center align-items-center flex-row p-1 mt-0 mt-md-4 mb-0 mb-md-2')
      .append($('<h2>').text('Saved content').addClass('mx-0 my-1'))
      .append(createRuqesMark().addClass(nonAggressiveCls).addClass('ml-2'))
    ;
    page.append(caption);

    const tabs = $(`
      <div class="btn-group w-100 mt-1">
        <button type="button" class="btn btn-primary ${saveTabButtonPostsCls}">Posts</button>
        <button type="button" class="btn btn-primary ${saveTabButtonCommentsCls}">Comments</button>
      </div>
    `);
    page.append(tabs);

    const commentsInnerContainer = $('<div>').addClass('card px-2 pb-2');
    pipe(
      (x: CommentSave[]) => reverse(x),
      map(renderSavedComment),
    )(cfg.save.comments).forEach(x => commentsInnerContainer.append(x));
    if (isEmpty(cfg.save.comments)) { commentsInnerContainer.append(this.genEmptySaves()); }
    const comments = $('<div>')
      .addClass(['row no-gutters mt-md-3', saveCommentsContainerCls])
      .append($('<div>').addClass('col-12').append(commentsInnerContainer));
    page.append(comments);

    const postsInnerContainer = $('<div>').addClass('posts');
    pipe(
      (x: PostSave[]) => reverse(x),
      map(renderSavedPost),
    )(cfg.save.posts).forEach(x => postsInnerContainer.append(x));
    if (isEmpty(cfg.save.posts)) { postsInnerContainer.append(this.genEmptySaves()); }
    const posts = $('<div>')
      .addClass(['row no-gutters mt-md-3', savePostsContainerCls])
      .append($('<div>').addClass('col-12').append(postsInnerContainer));
    page.append(posts);

    const setActiveTab = (tabName: 'posts' | 'comments'): void => {
      debugLog('setActiveTab', tabName);
      setClass($c(saveTabButtonPostsCls), 'active', tabName === 'posts');
      setClass($c(saveTabButtonCommentsCls), 'active', tabName === 'comments');
      setClass($c(savePostsContainerCls), 'd-none', tabName !== 'posts');
      setClass($c(saveCommentsContainerCls), 'd-none', tabName !== 'comments');
    };

    page.find(`.${saveTabButtonPostsCls}`).on('click', () => setActiveTab('posts'));
    page.find(`.${saveTabButtonCommentsCls}`).on('click', () => setActiveTab('comments'));

    $i('main-content-col').html(page);
    setActiveTab('posts');
    $('body').css({paddingTop: '42px'});
    await handleModulesAfterContentChange();
  }

  static isOnSavedPage() { return isOnRuqESUrl('saved'); }

  async setup(args: ModuleSetupArgs, cfg: Config) {
    const saveCfg = cfg.save;
    if (prop('silent', args || {})) {
      debugLog('SaveModule', saveCfg);
    }
    if (!cfg.save.enabled) { return; }
    if (!this.firstSetupRunFinished) { this.setupMainMenuButton(); }
    if (SaveModule.isOnSavedPage() && !this.firstSetupRunFinished) {
      await this.setupSavedPage(cfg);
    }
    if (cfg.save.postSavingEnabled) {
      this.setupSavePostActions(cfg);
    }
    if (cfg.save.commentSavingEnabled) {
      this.setupSaveCommentActions(cfg);
    }
  }

  async onContentChange(args: ModuleSetupArgs, cfg: Config): Promise<unknown> {
    return this.setup(args, cfg);
  }
}
