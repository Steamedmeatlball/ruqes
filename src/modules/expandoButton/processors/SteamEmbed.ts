import {EmbedProcessor, LinkProcessorArgs} from 'modules/expandoButton/EmbedProcessor';
import {ExpandoButtonModule} from 'modules/expandoButton/ExpandoButtonModule';

class SteamEmbed extends EmbedProcessor {
  protected genRegex(): RegExp | null {
    return /^https:\/\/store\.steampowered\.com\/app\/(\d+)\/?.*$/g;
  }

  protected async processMatchingLink({link, box}: LinkProcessorArgs): Promise<boolean> {
    const id = this.matchAllAndGetCapturedPart(link);
    const el = $(`
<div class="">
    <iframe src="https://store.steampowered.com/widget/${id}/" frameborder="0" width="646" height="190"></iframe>
</div>`);
    ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
    return true;
  }
}

export const steamEmbed = new SteamEmbed();
