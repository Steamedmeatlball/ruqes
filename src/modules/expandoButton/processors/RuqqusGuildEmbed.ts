import {boxGuildCls} from 'common/styles';
import {EmbedProcessor, LinkProcessorArgs} from 'modules/expandoButton/EmbedProcessor';
import {ExpandoButtonModule} from '../ExpandoButtonModule';

class RuqqusGuildEmbed extends EmbedProcessor {
  protected genRegex(): RegExp | null {
    return /^https:\/\/ruqqus\.com\/\+(\w+)$/g;
  }

  protected async processMatchingLink({link, box, cfg}: LinkProcessorArgs): Promise<boolean> {
    const guildName = this.matchAllAndGetCapturedPart(link);
    const img = $('<img>').prop('src', `/+${guildName}/pic/profile`);
    const guildEl = $('<a>').addClass('d-flex align-items-center flex-grow-1 justify-content-center p-3')
      .prop('href', link).append(img).append($('<strong>').text(`+${guildName}`).addClass('ml-2'));
    if (cfg.post.openInNewTab) { guildEl.prop('target', '_blank'); }
    const wrapper = $('<div>').addClass(boxGuildCls).addClass('d-flex align-items-center justify-content-center')
      // .append($('<span>').addClass('mr-3').text('Guild'))
      .append(guildEl);
    ExpandoButtonModule.insertWrappedElementIntoBox(box, wrapper);
    return true;
  }
}

export const ruqqusGuildEmbed = new RuqqusGuildEmbed();
