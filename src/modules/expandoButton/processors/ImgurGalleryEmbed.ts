import {drop, path} from 'ramda';

import {EmbedProcessor, LinkProcessorArgs} from 'modules/expandoButton/EmbedProcessor';
import {ExpandoButtonModule} from 'modules/expandoButton/ExpandoButtonModule';
import {debugLog, isArray, isString, printError, tryParseJSON, xhrP} from 'common/common';

class ImgurGalleryEmbed extends EmbedProcessor {
  private static readonly imgurGalleryPrefix = 'https://imgur.com/gallery/';
  private static imgurApiClientId = '3d83b5e5341b638';

  private static isImgurGallery = (x: string) => x.startsWith(ImgurGalleryEmbed.imgurGalleryPrefix);

  urlMatches(link: string): boolean {
    return ImgurGalleryEmbed.isImgurGallery(link);
  }

  protected async processMatchingLink({link: origLink, box, cfg}: LinkProcessorArgs): Promise<boolean> {
    try {
      const response = await xhrP({
        url: 'https://api.imgur.com/3/gallery/album/' + drop(ImgurGalleryEmbed.imgurGalleryPrefix.length)(origLink),
        method: 'GET',
        headers: {Authorization: `Client-ID ${ImgurGalleryEmbed.imgurApiClientId}`},
      });
      debugLog('handleImgurGallery', 'onload', response);
      const res = tryParseJSON(response.response);
      if (!res || !path(['success'])(res)) {
        ExpandoButtonModule.writeErrorToExpandButtonBox(box, 'Imgur responded with error.');
        return true;
      }
      debugLog('handleImgurGallery', 'parsed JSON, res =', res);
      const images = path(['data', 'images'])(res);
      if (!images || !isArray(images)) {
        ExpandoButtonModule.writeErrorToExpandButtonBox(box, 'Failed to extract "images" field.');
        return true;
      }
      const firstImage = images[0];
      const link = path(['link'])(firstImage);
      if (!link || !isString(link)) {
        ExpandoButtonModule.writeErrorToExpandButtonBox(box, 'Failed to get a link from image');
        return true;
      }
      ExpandoButtonModule.insertImageIntoBox(box, link, cfg)
        .prop('title', images.length > 1 ? `1 / ${images.length}` : '');
      return true;
    } catch (response) {
      printError('handleImgurGallery', 'failed', response);
      ExpandoButtonModule.writeErrorToExpandButtonBox(box, 'Imgur request failed: ' + response.status + ' - ' + String(response.response));
      return false;
    }
  }
}

export const imgurGalleryEmbed = new ImgurGalleryEmbed();
