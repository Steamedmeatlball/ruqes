import {EmbedProcessor, LinkProcessorArgs} from 'modules/expandoButton/EmbedProcessor';
import {ExpandoButtonModule} from 'modules/expandoButton/ExpandoButtonModule';

class ImgurGifvEmbed extends EmbedProcessor {
  protected genRegex(): RegExp | null {
    return /https:\/\/i\.imgur\.com\/(\w+)\.(gifv|mp4)/g;
  }

  protected async processMatchingLink({link, box, cfg}: LinkProcessorArgs): Promise<boolean> {
    const id = this.matchAllAndGetCapturedPart(link);
    const el = $(`<div class="handleImgurGifv"></div><blockquote class="imgur-embed-pub" lang="en" data-id="${id}"><a href="//imgur.com/${id}">?</a></blockquote><script async src="//s.imgur.com/min/embed.js" charset="utf-8"></script></div>`);
    ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
    return true;
  }
}

export const imgurGifvEmbed = new ImgurGifvEmbed();
