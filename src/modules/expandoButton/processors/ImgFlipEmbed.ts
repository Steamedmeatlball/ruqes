import {EmbedProcessor, LinkProcessorArgs} from 'modules/expandoButton/EmbedProcessor';
import {ExpandoButtonModule} from 'modules/expandoButton/ExpandoButtonModule';

class ImgFlipEmbed extends EmbedProcessor {
  protected genRegex(): RegExp | null {
    return /^https:\/\/imgflip\.com\/i\/.*?([a-zA-Z0-9]+)$/gm;
  }

  protected async processMatchingLink({link, box, cfg}: LinkProcessorArgs): Promise<boolean> {
    const id = this.matchAllAndGetCapturedPart(link);
    const src = `https://i.imgflip.com/${id}.jpg`;
    ExpandoButtonModule.insertImageIntoBox(box, src, cfg);
    return true;
  }
}

export const imgFlipEmbed = new ImgFlipEmbed();
