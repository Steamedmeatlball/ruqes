import {EmbedProcessor, LinkProcessorArgs} from 'modules/expandoButton/EmbedProcessor';
import {ExpandoButtonModule} from 'modules/expandoButton/ExpandoButtonModule';

class BitChuteEmbed extends EmbedProcessor {
  protected genRegex(): RegExp | null {
    return /^https:\/\/www\.bitchute\.com\/video\/(\w+).*$/g;
  }

  protected async processMatchingLink({link, box, cfg}: LinkProcessorArgs): Promise<boolean> {
    const id = this.matchAllAndGetCapturedPart(link);
    if (!id) {
      console.log('failed to get BitChute video id', link);
      return false;
    }
    const el = $(`<div class="handleBitChuteVideo embed-responsive embed-responsive-16by9"><iframe width="640" height="360" scrolling="no" frameborder="0" style="border: none;" src="https://www.bitchute.com/embed/${id}/"></iframe></div>`);
    ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
    return true;
  }
}

export const bitChuteEmbed = new BitChuteEmbed();
