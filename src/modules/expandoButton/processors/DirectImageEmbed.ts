import {EmbedProcessor, LinkProcessorArgs} from 'modules/expandoButton/EmbedProcessor';
import {ExpandoButtonModule} from 'modules/expandoButton/ExpandoButtonModule';

class DirectImageEmbed extends EmbedProcessor {
  protected genRegex(): RegExp | null {
    return /\.(?:jpg|jpeg|png|gif|svg|webp)(?:\?.*)?$|^https:\/\/i.ruqqus.com\//i;
  }

  protected async processMatchingLink({link, box, cfg}: LinkProcessorArgs): Promise<boolean> {
    ExpandoButtonModule.insertImageIntoBox(box, link, cfg);
    return true;
  }

  // This was briefly needed when links were set to `javascript:void(0)`.
  // if (fromExpandDesktopImageCall) {
  //   ExpandoButtonModule.insertImageIntoBox(box, fromExpandDesktopImageCall.image, cfg);
  //   return true;
  // }
}

export const directImageEmbed = new DirectImageEmbed();
