import {EmbedProcessor, LinkProcessorArgs} from 'modules/expandoButton/EmbedProcessor';
import {ExpandoButtonModule} from 'modules/expandoButton/ExpandoButtonModule';
import {xhrP} from 'common/common';

class IbbcoEmbed extends EmbedProcessor {
  protected genRegex(): RegExp | null {
    return /^https:\/\/ibb.co\//gm;
  }

  protected async processMatchingLink({link, box, cfg}: LinkProcessorArgs): Promise<boolean> {
    try {
      const resp = await xhrP({
        method: 'GET',
        url: link,
      });

      const parsed = $(resp.response);
      const img = parsed.find('#image-viewer-container img');
      const imgUrl = img.prop('src');
      ExpandoButtonModule.insertImageIntoBox(box, imgUrl, cfg);
    } catch (err) {
      this.handleResponseError(box, err);
    }
    return true;
  }
}

export const ibbcoEmbed = new IbbcoEmbed();
