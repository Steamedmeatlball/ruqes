import {EmbedProcessor, LinkProcessorArgs} from 'modules/expandoButton/EmbedProcessor';
import {ExpandoButtonModule} from 'modules/expandoButton/ExpandoButtonModule';

class ParlerEmbed extends EmbedProcessor {
  protected genRegex(): RegExp | null {
    return /^https:\/\/parler\.com\/post\/(\w+).*$/g;
  }

  protected async processMatchingLink({link, box, cfg}: LinkProcessorArgs): Promise<boolean> {
    const id = this.matchAllAndGetCapturedPart(link);
    if (!id) {
      console.log('failed to get Parler video id', link);
      return false;
    }
    const el = $(`
<blockquote class="parler-parley">
  <a lang="en" dir="ltr" href="https://embed.parler.com/post/${id}" target="_blank"></a>
</blockquote>
<script async src="https://embed.parler.com/embed/parley-embed.min.js" charset="utf-8"></script>
`);
    ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
    return true;
  }
}

export const parlerEmbed = new ParlerEmbed();
