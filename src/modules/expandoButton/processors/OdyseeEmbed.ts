import {EmbedProcessor, LinkProcessorArgs} from 'modules/expandoButton/EmbedProcessor';
import {ExpandoButtonModule} from '../ExpandoButtonModule';
import {debugLog, printError, xhrP} from 'common/common';

class OdyseeEmbed extends EmbedProcessor {
  protected genRegex(): RegExp | null {
    return /^https:\/\/odysee.com\/@[\w:]+\/[\w\-:]+$/;
  }

  protected async processMatchingLink({link, box, cfg}: LinkProcessorArgs): Promise<boolean> {
    try {
      const resp = await xhrP({
        method: 'GET',
        url: link,
      });
      debugLog('handleOdyseeLink', resp);
      const parsed = $('<div>').append($(resp.response));
      const videoUrl = parsed.find('meta[property="og:video"]').prop('content');
      const el = $(`<div class="handleOdyseeLink embed-responsive embed-responsive-16by9"><iframe src="${videoUrl}" allowfullscreen></iframe></div>`);
      ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
      return true;
    } catch (err) {
      printError('OdyseeEmbed', err);
      return false;
    }
  }
}

export const odyseeEmbed = new OdyseeEmbed();
