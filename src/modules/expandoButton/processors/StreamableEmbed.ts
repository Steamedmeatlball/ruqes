import {EmbedProcessor, LinkProcessorArgs} from 'modules/expandoButton/EmbedProcessor';
import {ExpandoButtonModule} from 'modules/expandoButton/ExpandoButtonModule';

class StreamableEmbed extends EmbedProcessor {
  protected genRegex(): RegExp | null {
    return /^https:\/\/streamable\.com\/([\w\d]+).*$/g;
  }

  protected async processMatchingLink({link, box}: LinkProcessorArgs): Promise<boolean> {
    const id = this.matchAllAndGetCapturedPart(link);
    const el = $(`
<div class="embed-responsive embed-responsive-16by9">
    <iframe src="https://streamable.com/e/${id}" frameborder="0"></iframe>
</div>`);
    ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
    return true;
  }
}

export const streamableEmbed = new StreamableEmbed();
