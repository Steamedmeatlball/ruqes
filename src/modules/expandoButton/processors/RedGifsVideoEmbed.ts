import {EmbedProcessor, LinkProcessorArgs} from 'modules/expandoButton/EmbedProcessor';
import {ExpandoButtonModule} from 'modules/expandoButton/ExpandoButtonModule';

class RedGifsVideoEmbed extends EmbedProcessor {
  protected genRegex(): RegExp | null {
    return /^https:\/\/(?:www\.)?redgifs\.com\/watch\/([\w_\d\-]+).*$/g;
  }

  protected async processMatchingLink({link, box, cfg}: LinkProcessorArgs): Promise<boolean> {
    const id = this.matchAllAndGetCapturedPart(link);
    if (!id) {
      console.log('failed to get RedGifs video id', link);
      return false;
    }
    const el = $(`<div class="handleRedGifsVideo embed-responsive embed-responsive-16by9"><iframe src='https://redgifs.com/ifr/${id}' frameborder='0' scrolling='no' allowfullscreen width='640' height='360'></iframe></div>`);
    ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
    return true;
  }
}

export const redGifsVideoEmbed = new RedGifsVideoEmbed();
