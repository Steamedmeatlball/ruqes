import {clamp, identity, merge, path, prop} from 'ramda';

import {Config} from 'common/config';
import {
  debugLog,
  extractPostInfo,
  genJsAnchor,
  getElSize,
  getWindowEl,
  isArray,
  isNumber,
  isOnPostDetail,
  isOnPostEmbed,
  isString,
  printError, printLog,
  scrollIntoViewIfNeeded,
} from 'common/common';
import {
  boxEmbedCls,
  boxEmptyCls,
  boxImgCls,
  boxPostCommentsWithButtonCls,
  boxPostTextCls,
  errorTextCls,
  expandBoxCloserCls,
  expandBoxCls,
  expandBoxOpenedCls,
  expandoBtnCls,
  expandoStylesCls,
  resizableDirectImageCls,
  resizableDirectImageWrapperCls,
} from 'common/styles';
import {getPostActionsList, getPosts} from 'common/selectors';
import {handleTextOfPost} from 'modules/expandoButton/textOfPostProcessor';
import {handleModulesAfterContentChange, ModuleSetupArgs} from 'common/modules';
import {RuqESModule} from 'common/RuqESModule';
import {addStyle} from 'common/domUtils';
import {EmbedProcessor, LinkProcessorArgs} from './EmbedProcessor';
import {twitchClipEmbed} from './processors/TwitchClipEmbed';
import {ruqqusGuildEmbed} from './processors/RuqqusGuildEmbed';
import {lbryEmbed} from './processors/LbryEmbed';
import {metaEmbed} from './processors/MetaEmbed';
import {steamEmbed} from './processors/SteamEmbed';
import {videoEmbed} from './processors/VideoEmbed';
import {streamableEmbed} from './processors/StreamableEmbed';
import {twitterEmbed} from './processors/TwitterEmbed';
import {ibbcoEmbed} from './processors/IbbcoEmbed';
import {imgFlipEmbed} from './processors/ImgFlipEmbed';
import {imgurGifvEmbed} from './processors/ImgurGifvEmbed';
import {gfycatVideoEmbed} from './processors/GfycatVideoEmbed';
import {gifDeliveryNetworkEmbed} from './processors/GifDeliveryNetworkEmbed';
import {redGifsVideoEmbed} from './processors/RedGifsVideoEmbed';
import {bitChuteEmbed} from './processors/BitChuteEmbed';
import {youTubeEmbed} from './processors/YouTubeEmbed';
import {imgurGalleryEmbed} from './processors/ImgurGalleryEmbed';
import {directImageEmbed} from './processors/DirectImageEmbed';
import {ruqqusPostEmbed} from './processors/RuqqusPostEmbed';
import {imgurAlbumEmbed} from './processors/ImgurAlbumEmbed';
import {imgurBrokenEmbed} from './processors/ImgurBrokenEmbed';
import {odyseeEmbed} from './processors/OdyseeEmbed';
import {parlerEmbed} from './processors/ParlerEmbed';

interface MaxRecommendedImageSizeArgs {
  contentXPercent: number;
  contentYPercent: number;
}

const defaultMaxRecommendedImageSizeArgs: MaxRecommendedImageSizeArgs = {
  contentXPercent: 0.999,
  contentYPercent: 0.66,
};

export class ExpandoButtonModule extends RuqESModule {
  private static embedProcessors: EmbedProcessor[] = [
    directImageEmbed,
    ruqqusPostEmbed,
    imgurBrokenEmbed,
    imgurAlbumEmbed,
    imgurGalleryEmbed,
    youTubeEmbed,
    bitChuteEmbed,
    redGifsVideoEmbed,
    gifDeliveryNetworkEmbed,
    gfycatVideoEmbed,
    imgurGifvEmbed,
    imgFlipEmbed,
    ibbcoEmbed,
    twitterEmbed,
    lbryEmbed,
    odyseeEmbed,
    ruqqusGuildEmbed,
    twitchClipEmbed,
    streamableEmbed,
    steamEmbed,
    parlerEmbed,
    videoEmbed,
    metaEmbed,
  ];

  private createExpandoButton = (cfg: Config) => {
    const el = genJsAnchor()
      .text(cfg.expandoButton.textClosed)
      .addClass(expandoBtnCls)
      .addClass(`${expandoBtnCls}--style-${cfg.expandoButton.style}`)
      .addClass('btn')
      .addClass('btn-secondary')
      .prop('role', 'button');
    if (cfg.expandoButton.hide) { el.hide(); }
    return el;
  };

  private createBox = (cfg: Config) => {
    const el = $('<div>').addClass(expandBoxCls).addClass(boxEmptyCls);
    if (cfg.expandoButton.closerEnabled) {
      const closerEl = $('<div>').addClass(expandBoxCloserCls).click((evt: JQuery.ClickEvent) => {
        debugLog('createBox', 'closerEl', 'click', evt);
        const postEl = $(evt.target).parent().parent();
        const expandoBtn = postEl.find(`.${expandoBtnCls}`).first();
        if (!expandoBtn.length) {
          printError('Expando closer failed to locate the expando button.');
          return;
        }
        debugLog({postEl, expandoBtn});
        expandoBtn.click();
        scrollIntoViewIfNeeded(postEl.prev());
      });
      el.append(closerEl);
    }
    return el;
  };

  static insertWrappedElementIntoBox = (box: JQuery, element: JQuery | JQuery[], wrapperClass: string = boxEmbedCls) => {
    const wrapped = $('<div>').addClass(wrapperClass);
    const toAdd = isArray(element) ? element : [element];
    toAdd.forEach(x => wrapped.append(x));
    ExpandoButtonModule.insertRawElementIntoBox(box, wrapped);
    handleModulesAfterContentChange();
  };

  static insertRawElementIntoBox = (box: JQuery, element: JQuery) => {
    box.append(element);
  };

  static writeErrorToExpandButtonBox = (box: JQuery, errorText: string, retryCb: (() => void) | null = null) => {
    const errorEl = $('<div>')
      .addClass(errorTextCls)
      .append($('<span>').text(errorText))
    ;
    if (retryCb) {
      const retryButton = $('<button>')
        .text('Retry')
        .addClass('btn')
        .addClass('btn-primary')
        .addClass('ml-1')
        .click(() => {
          box.find(`.${errorTextCls}`).remove();
          retryCb();
        });
      errorEl.append(retryButton);
    }
    box.prepend(errorEl);
  };

  static setupResizing = (el: JQuery) => {
    interface ResizingState {
      resizing: boolean;
      startPoint: [number, number] | null;
      startValue: number;
      value: number;
    }

    const finishSetup = (firstRun: boolean) => {
      debugLog('setupResizing', 'finishSetup');
      const elW = el.width();
      const elPW = el.parent().innerWidth();
      const initValue = elW / elPW * 100;
      debugLog('setupResizing', 'finishSetup', {initValue, elW, elPW});
      if (!elW || !elPW) {
        if (firstRun) {
          setTimeout(() => finishSetup(false), 10);
        } else {
          printError('Failed to setup resizing for ', el.toArray());
        }
        return;
      }
      el.css({maxWidth: 'none', maxHeight: 'none', width: `${initValue}%`});
      el.addClass(resizableDirectImageCls);
      el.parent().addClass(resizableDirectImageWrapperCls);

      const state: ResizingState = {
        resizing: false,
        startPoint: null,
        startValue: initValue,
        value: initValue,
      };
      el.mousedown(e => {
        if (e.button !== 0) { return; }
        state.resizing = true;
        state.startPoint = [e.clientX, e.clientY];
        state.startValue = state.value;
      });
      const doc = $(document);
      doc.mouseup(e => {
        state.resizing = false;
        state.startPoint = null;
      });
      doc.mousemove(e => {
        if (!state.resizing || !state.startPoint) {
          return;
        }
        const nx = e.clientX;
        const ny = e.clientY;
        const [ox, oy] = state.startPoint;
        const dx = nx - ox;
        const dy = ny - oy;
        const dist = dx + dy;
        state.value = clamp(5, 500, state.startValue + dist / 5);
        el.css({width: `${state.value}%`});
      });
    };

    debugLog('setupResizing', {naturalWidth: el.prop('naturalWidth'), complete: el.prop('complete')}, el);
    if (el.prop('complete') && el.prop('naturalWidth') !== 0) {
      finishSetup(true);
    } else {
      el.on('load', () => finishSetup(true));
    }
  };


  static getMaxRecommendedImageSize = (parent: JQuery, args: Partial<MaxRecommendedImageSizeArgs> = {}): [number, number] => {
    const opts: MaxRecommendedImageSizeArgs = merge(defaultMaxRecommendedImageSizeArgs)(args);
    if (!isNumber(opts.contentXPercent) || opts.contentXPercent < 0 || opts.contentXPercent > 1) {
      throw new Error(`Invalid opts.contentXPercent = ${opts.contentXPercent}`);
    }
    if (!isNumber(opts.contentYPercent) || opts.contentYPercent < 0 || opts.contentYPercent > 1) {
      throw new Error(`Invalid opts.contentYPercent = ${opts.contentYPercent}`);
    }
    const winSize = getElSize(getWindowEl());
    const parentSize = getElSize(parent);
    return [parentSize[0] * opts.contentXPercent, winSize[1] * opts.contentYPercent];
  };

  static genImageForBox = (box: JQuery, link: string, cfg: Config, type: 'expando' | 'metaCard'): JQuery => {
    const el = $('<img>').prop('src', link).prop('draggable', false).addClass(boxImgCls).css({maxWidth: '1px'});
    const maxSize = type === 'metaCard' ? {contentXPercent: 0.95} : undefined;
    const recSize = ExpandoButtonModule.getMaxRecommendedImageSize(box, maxSize);
    debugLog('genImageForBox', box.width(), recSize, maxSize);
    el.css({
      maxWidth: recSize[0],
      maxHeight: recSize[1],
    });
    if (cfg?.expandoButton?.resize) {
      setTimeout(() => ExpandoButtonModule.setupResizing(el));
    }
    return el;
  };

  static insertImageIntoBox = (box: JQuery, link: string, cfg: Config): JQuery => {
    const el = ExpandoButtonModule.genImageForBox(box, link, cfg, 'expando');
    ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
    return el;
  };

  static readonly genExpandDesktopImageCallRegex = () => /expandDesktopImage\('(.*)','(.*)'\)/g;

  static extractExpandDesktopImage(el: JQuery): LinkProcessorArgs['fromExpandDesktopImageCall'] {
    const handler = el.find('.post-img').attr('onclick');
    if (!handler || !isString(handler)) { return; }
    const matches = [...handler.matchAll(this.genExpandDesktopImageCallRegex())];
    const image = path([0, 1], matches);
    const link = path([0, 2], matches);
    if (!isString(image) || !isString(link)) { return; }
    return {image, link};
  }

  private genButtonClickHandler =
    (el: JQuery<HTMLElement>, link: string, postUrl: string, cfg: Config) =>
      async (evt: JQuery.Event) => {
        debugLog('genButtonClickHandler', evt, el, link, postUrl, cfg);
        evt.stopPropagation();
        const box = el.find(`.${expandBoxCls}`);
        const boxOpened = box.hasClass(expandBoxOpenedCls);
        const btn = $((evt as any).target);
        if (boxOpened) {
          btn.text(cfg.expandoButton.textClosed);
          box.removeClass(expandBoxOpenedCls);
          return;
        }
        btn.text(cfg.expandoButton.textOpened);
        box.addClass(expandBoxOpenedCls);
        if (box.hasClass(boxEmptyCls)) {
          box.removeClass(boxEmptyCls);
          if (!isOnPostDetail() && !isOnPostEmbed()) { handleTextOfPost(postUrl, box, cfg); }
          const fromExpandDesktopImageCall = ExpandoButtonModule.extractExpandDesktopImage(el);
          const linkProcessorArgs: LinkProcessorArgs = {
            box,
            cfg,
            link,
            postEl: el,
            fromExpandDesktopImageCall,
            postInfo: extractPostInfo(el),
          };
          const processed = await ExpandoButtonModule.embedProcessors.reduce(
            async (acc, x) => await acc || await x.process(linkProcessorArgs),
            Promise.resolve(linkProcessorArgs.postInfo.textPost),
          );
          if (!processed) {
            const unsupportedEl = $('<div>')
              .append('Unsupported link type: ')
              .append($('<code>').html($('<a>').prop('href', link).text(link)))
              .append('.');
            ExpandoButtonModule.insertWrappedElementIntoBox(box, unsupportedEl);
          }
        }
      };

  private processPostItem = (cfg: Config) => (_: unknown, domEl: HTMLElement) => {
    const el = $(domEl);
    const link = el.find('.card-header a').prop('href') || el.find('.post-title a').prop('href');
    const postUrl = el.find('.card-block .post-title a').prop('href');
    if (!link) { return; }
    el.append(this.createBox(cfg));
    const btn = this.createExpandoButton(cfg);
    btn.click(this.genButtonClickHandler(el, link, postUrl, cfg));
    const actions = getPostActionsList(el);
    if (cfg.expandoButton.alignRight) {
      actions.append(btn);
    } else {
      actions.prepend(btn);
    }
    if (cfg.expandoButton.clickOnPostToOpen) {
      el.find('.card-block .post-title a').click((evt: JQuery.ClickEvent) => {
        printLog('ExpandoButtonModule', 'clickOnPostToOpen', link, evt);
        const titleEl = $(evt.target);
        const selSuffix = cfg.expandoButton.hide ? '' : ':visible';
        let currentExpandoButtonEl = titleEl.parent().parent().find(`.${expandoBtnCls}${selSuffix}`);
        if (currentExpandoButtonEl.length !== 1) {
          currentExpandoButtonEl = titleEl.parent().parent().parent().parent().find(`.${expandoBtnCls}${selSuffix}`);
        }
        evt.stopPropagation();
        evt.preventDefault();
        if (currentExpandoButtonEl.length !== 1) {
          printError('Invalid number of expando buttons for clickOnPostToOpen handler.', currentExpandoButtonEl, titleEl);
          return;
        }
        currentExpandoButtonEl.click();
      });
    }
    if (isOnPostDetail()) {
      const isUnsupportedVideoEmbed = [
        bitChuteEmbed,
        lbryEmbed,
        twitchClipEmbed,
        streamableEmbed,
        videoEmbed,
      ].reduce((a, x) => a || x.urlMatches(link), false);
      const openBecauseUnsupported = cfg.expandoButton.autoOpenOnDetailOnUnsupportedVideos && isUnsupportedVideoEmbed;
      const openFromAutoOpenAllOnDetail = cfg.expandoButton.autoOpenOnDetail;
      const dontOpenBecauseSupported = [
        youTubeEmbed,
        twitterEmbed,
      ].map(x => x.urlMatches(link)).some(identity);
      if ((openBecauseUnsupported || openFromAutoOpenAllOnDetail) && !dontOpenBecauseSupported) {
        btn.trigger('click');
      }
    } else {
      if (cfg.expandoButton.autoOpen) {
        btn.trigger('click');
      }
    }
  };

  private setupStyles(cfg: Config): void {
    const styles = `
      .${boxPostTextCls} { order: ${cfg.expandoButton.postTextOrder}; }
      .${boxEmbedCls} { order: ${cfg.expandoButton.embedOrder}; }
      .${boxPostCommentsWithButtonCls} { order: ${cfg.expandoButton.commentsOrder}; }
    `.trimEnd();
    addStyle(styles, expandoStylesCls);
  }

  async setup(args: ModuleSetupArgs, cfg: Config) {
    if (!cfg?.expandoButton?.enabled) { return; }
    if (!prop('silent', args || {})) { debugLog('setupExpandoButton'); }
    getPosts().filter((_, el) => $(el).find(`.${expandoBtnCls}`).length === 0).each(this.processPostItem(cfg));
    if (!this.firstSetupRunFinished) { this.setupStyles(cfg); }
  };

  async onContentChange(args: ModuleSetupArgs, cfg: Config): Promise<unknown> {
    return this.setup(args, cfg);
  }
}
