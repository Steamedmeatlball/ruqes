import {createTextLoader, genJsAnchor, printError, setupVoting, xhr, xhrP} from 'common/common';
import {
  boxPostTextCls,
  postLoadingCls,
  boxPostCommentsCls,
  boxPostCommentsWithButtonCls,
} from 'common/styles';
import {Config} from 'common/config';
import {ExpandoButtonModule} from 'modules/expandoButton/ExpandoButtonModule';
import {ruqqusPostEmbed} from './processors/RuqqusPostEmbed';

const onFetchingPostFailed = (postUrl: string, box: JQuery, cfg: Config) => (resp: GM.Response<unknown>) => {
  console.error('Failed to fetch a post.', resp);
  box.find(`.${postLoadingCls}`).remove();
  ExpandoButtonModule.writeErrorToExpandButtonBox(
    box,
    `Failed to fetch the post. Error ${resp.status}: ${resp.statusText}`,
    () => handleTextOfPost(postUrl, box, cfg),
  );
};

const onFetchingPostSucceeded = async function (resp: GM.Response<unknown>, postUrl: string, box: JQuery<HTMLElement>, cfg: Config) {
  if (resp.status !== 200) {
    return onFetchingPostFailed(postUrl, box, cfg)(resp);
  }
  const parsed = $(resp.response);
  const post = parsed.find('#post-body').children().filter(':not(.embed-responsive)');
  box.find(`.${postLoadingCls}`).remove();
  const emptyPost = !post.length || post.text().trim() === '';
  const comments = cfg.expandoButton.showComments ? parsed.find('.comment-section:not(.text-center) > .comment') : $();
  const emptyComments = comments.length === 0;
  if (emptyPost && emptyComments) { return; }
  const wrappedComments = $('<div>').addClass(boxPostCommentsCls).append(comments).hide();
  setupVoting(wrappedComments);
  const showCommentsButton = $('<a>').addClass('btn btn-secondary').text('Toggle comments').click(evt => {
    const el = $(evt.target);
    el.parent().find(`.${boxPostCommentsCls}`).toggle();
  });
  if (cfg.expandoButton.hideToggleCommentsButton) {
    showCommentsButton.hide();
  }

  ExpandoButtonModule.insertWrappedElementIntoBox(box, post, boxPostTextCls);
  if (!emptyComments) {
    ExpandoButtonModule.insertWrappedElementIntoBox(box, [
      showCommentsButton,
      wrappedComments,
    ], boxPostCommentsWithButtonCls);
  }

  if (cfg.expandoButton.autoExpandComments) { showCommentsButton.click(); }
};

const doRequest = async function (postUrl: string, box: JQuery<HTMLElement>, cfg: Config) {
  try {
    const resp = await xhrP({method: 'GET', url: postUrl});
    await onFetchingPostSucceeded(resp, postUrl, box, cfg);
  } catch (err) {
    onFetchingPostFailed(postUrl, box, cfg);
  }
};

const startFetchingPost = async (box: JQuery<HTMLElement>, postUrl: string, cfg: Config) => {
  box.append(createTextLoader('Fetching post', postLoadingCls).addClass('m-2'));
  await doRequest(postUrl, box, cfg);
};

const createLoadPostButton = async (postUrl: string, box: JQuery, cfg: Config) => {
  const btn = genJsAnchor().addClass('btn btn-secondary mb-1').text('Load post').click(async () => {
    btn.remove();
    await startFetchingPost(box, postUrl, cfg);
  });
  box.append(btn);
};

export const handleTextOfPost = async (postUrl: string, box: JQuery, cfg: Config) => {
  if (!ruqqusPostEmbed.urlMatches(postUrl)) { printError('handleTextOfPost', 'invalid post URL', postUrl); }
  if (cfg.expandoButton.deferLoadingOfPost || cfg.expandoButton.autoOpen) {
    await createLoadPostButton(postUrl, box, cfg);
  } else {
    await startFetchingPost(box, postUrl, cfg);
  }
};
