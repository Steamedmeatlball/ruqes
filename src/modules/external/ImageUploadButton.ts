import {
  imageUploadInputCls,
  uploadImageToImgbbButtonCls,
  uploadImageToImgbbButtonHighlightedCls,
  uploadImageToImgbbButtonLoadingCls, uploadImageToImgbbButtonSmallCls,
} from '../../common/styles';
import logoSvg from '../../assets/logo.svg';
import {$c, createRuqesMark, createSmallCommentButton, debugLog, preventDefaults} from '../../common/common';
import {uploadImageToImgBb} from './ImgBbHelpers';
import {Config} from 'common/config';
import Match from 'ts-matcher';

type UploadButtonType = 'POST' | 'COMMENT';

type ImageUploadSuccessHandler = (url: string) => void;

export class ImageUploadButton {
  static addLoadingEffectsToButton = (btn: JQuery) => {
    btn.prop('disabled', true).addClass(uploadImageToImgbbButtonLoadingCls);
  };

  static removeLoadingEffectsFromButton = (btn: JQuery) => {
    btn.prop('disabled', false).removeClass(uploadImageToImgbbButtonLoadingCls);
  };

  static genUploadButton = (type: UploadButtonType, cfg: Config, onSuccess: ImageUploadSuccessHandler): JQuery => {
    const btn = Match(type)
      .case('POST', () =>
        $('<a>')
          .addClass('btn btn-secondary')
          .html(logoSvg)
          .append('Upload image')
          .addClass(uploadImageToImgbbButtonCls),
      )
      .case('COMMENT', () =>
        createSmallCommentButton('img')
          .addClass([uploadImageToImgbbButtonCls, uploadImageToImgbbButtonSmallCls])
          .prop('title', `[RuqES] Upload an image to the configured image service and insert corresponding markdown code`),
      )
      .exec();
    const removeHighlight = () => btn.removeClass(uploadImageToImgbbButtonHighlightedCls);
    const onDropOnImgBbUploadButton = (evt: Event) => {
      preventDefaults(evt);
      if (btn.hasClass(uploadImageToImgbbButtonLoadingCls)) {
        debugLog('genUploadButton - ignoring, loading already in progress', evt);
        return;
      }
      const dt = (evt as any).dataTransfer || (evt as any).originalEvent.dataTransfer;
      const files = [...dt.files];
      debugLog('genUploadButton', 'drop', dt || 'missing DT', files || 'missing files', evt);
      removeHighlight();
      ImageUploadButton.addLoadingEffectsToButton(btn);
      const file = files[0];
      if (file) {
        uploadImageToImgBb({
          cfg,
          file,
          onError: resp => {
            ImageUploadButton.removeLoadingEffectsFromButton(btn);
            alert(`drop upload failed: ${resp.status}\n${resp.response}`);
          },
          onSuccess: url => {
            ImageUploadButton.removeLoadingEffectsFromButton(btn);
            onSuccess(url);
          },
        });
      }
    };
    btn
      .on('dragover', evt => {
        preventDefaults(evt);
        btn.addClass(uploadImageToImgbbButtonHighlightedCls);
      })
      .on('dragleave', evt => {
        preventDefaults(evt);
        removeHighlight();
      })
      .on('drop', onDropOnImgBbUploadButton)
    ;
    btn.on('click', ImageUploadButton.genClickHandler(cfg, btn, onSuccess));
    return btn;
  };

  private static genClickHandler = (cfg: Config, el: JQuery, onSuccess: ImageUploadSuccessHandler) => (evt: JQuery.Event) => {
    debugLog('genClickHandler', cfg, el, evt);
    if (el.hasClass(uploadImageToImgbbButtonLoadingCls)) {
      debugLog('genClickHandler - ignoring click, loading already in progress');
      return;
    }

    $c(imageUploadInputCls).remove();
    const fileInput = $('<input type="file" class="d-none" accept="image/x-png,image/gif,image/jpeg">').addClass(imageUploadInputCls);

    const enable = () => ImageUploadButton.removeLoadingEffectsFromButton(el);
    const onError = (err: GM.Response<unknown>) => {
      debugLog('onError', err);
      alert(`Upload failed: ${err.status}\n${err.response}`);
      enable();
    };

    fileInput.change(evt => {
      debugLog('fileInput.change', evt);
      const files = fileInput.prop('files');
      if (!files || !files[0]) { return; }
      ImageUploadButton.addLoadingEffectsToButton(el);
      const file = files[0];
      debugLog('got pic for upload', file);
      if (file) {
        uploadImageToImgBb({
          cfg,
          file,
          onSuccess: url => {
            onSuccess(url);
            enable();
          },
          onError,
        });
      }
    });
    $('body').append(fileInput);
    fileInput.click();
  };
}
