import {prop} from 'ramda';

import {getNextPageLink, getPageLoadingMarker, getPagination, getPostListContainer} from 'common/selectors';
import {
  $c,
  createTextLoader,
  debugLog,
  logPrefix,
  printError,
  xhr,
  $i,
  setupVoting
} from 'common/common';
import {Config} from 'common/config';
import {
  clsPrefix,
  codeBlockCls,
  infiniteScrollCoolDownMarkerCls,
  pageLoadingCls,
  pageLoadingErrorCls
} from 'common/styles';
import {handleModulesAfterContentChange, ModuleSetupArgs} from 'common/modules';
import {RuqESModule} from '../common/RuqESModule';
import {emitInfiniteScrollLoaded, emitInfiniteScrollLoadFail, emitInfiniteScrollLoadStart} from '../common/events';

const coolDownWhenCombinedWithFullHidingOfPosts = 1500;

interface InfiniteScrollState {
  loading: boolean,
  nextPageUrl: string | null,
  coolDownActive: boolean,
  coolDownTimeoutId: number | null,
  loadNextPageAfterCoolDownFinishes: boolean,
}

export class InfiniteScrollModule extends RuqESModule {
  private infiniteScrollState: InfiniteScrollState = {
    loading: false,
    nextPageUrl: null,
    coolDownActive: false,
    coolDownTimeoutId: null,
    loadNextPageAfterCoolDownFinishes: false,
  };

  static parsePageNumber(url?: string | null): number | null {
    if (!url) { return null; }
    const parsed = Number.parseInt(prop(1, [...url?.match(/page=(\d+)/)]));
    return isNaN(parsed) ? null : parsed;
  }

  private onNextPageSuccessResponse = async (resp: GM.Response<unknown>) => {
    if (resp.status !== 200) {
      return this.onNextPageErrorResponse(resp);
    }
    const parsed = $(resp.response);
    debugLog('onNextPageSuccessResponse', resp);
    let newData = parsed.find('#posts');
    if (!newData.length) {
      newData = parsed.find('.guild-border-top').next();
    }
    if (!newData.length) {
      newData = parsed.find('.posts');
    }
    if (!newData.length) {
      printError('InfiniteScroll failed to locate data in response.', parsed);
      getPageLoadingMarker().html('[RuqES] Failed to locate data in response.');
      getPagination().css('display', 'block');
      return;
    }
    const pageNum = InfiniteScrollModule.parsePageNumber(this.infiniteScrollState.nextPageUrl) ?? -1;
    const newPageId = `${clsPrefix}page-${pageNum}`;
    const newPage = $('<div>')
      .prop('id', newPageId)
      .html(newData)
      .append(getPagination(parsed));
    setupVoting(newPage)
    const pageCounter = $('<div>').text(`Page ${pageNum || '?'}`)
      .addClass(['card', 'text-center', 'py-1']);
    getPagination().remove();
    getPostListContainer().append(pageCounter);
    getPostListContainer().append(newPage);
    getPageLoadingMarker().remove();
    this.infiniteScrollState.loading = false;
    this.infiniteScrollState.nextPageUrl = null;
    await handleModulesAfterContentChange();
    emitInfiniteScrollLoaded(pageNum, resp.finalUrl, $i(newPageId)[0]);
  };

  private onNextPageErrorResponse = (resp: GM.Response<unknown>) => {
    console.error(logPrefix, 'loading of next page FAILED', resp);
    const textEl = $('<pre>').addClass(codeBlockCls).html($('<code>').text(resp.responseText));
    const tryManuallyBtn = $('<a>')
      .text('Go to next page manually')
      .prop('href', this.infiniteScrollState.nextPageUrl);
    const retryBtn = $('<button>')
      .text('Retry')
      .addClass('btn')
      .addClass('btn-primary')
      .addClass('ml-1')
      .click(() => {
        this.infiniteScrollState.loading = false;
        getPageLoadingMarker().remove();
        this.startLoadingNextPage();
      });
    const errorEl = $('<div>')
      .addClass(pageLoadingErrorCls)
      .append($('<div>').text(`Error ${resp.status}: ${resp.statusText}`).append(retryBtn))
      .append(tryManuallyBtn)
      .append(textEl)
    ;
    getPageLoadingMarker().html(errorEl);
    emitInfiniteScrollLoadFail(resp.finalUrl, resp.status, resp.statusText)
  };

  private startLoadingNextPage = () => {
    if (this.infiniteScrollState.coolDownActive) {
      this.infiniteScrollState.loadNextPageAfterCoolDownFinishes = true;
      if ($c(infiniteScrollCoolDownMarkerCls).length === 0) {
        const cdMarker = $('<span>⏳</span>').addClass(infiniteScrollCoolDownMarkerCls);
        getPagination().find('ul').append(cdMarker);
      }
      return;
    }
    const nextUrl = getNextPageLink().prop('href');
    debugLog('InfiniteScroll starts fetching', nextUrl);
    if (!nextUrl) {
      return;
    }
    this.infiniteScrollState.loading = true;
    this.infiniteScrollState.nextPageUrl = nextUrl;
    const pageNumber = InfiniteScrollModule.parsePageNumber(nextUrl);
    if (pageNumber !== null) { emitInfiniteScrollLoadStart(pageNumber, nextUrl); }
    xhr({
      method: 'GET',
      url: nextUrl,
      onload: this.onNextPageSuccessResponse,
      onerror: this.onNextPageErrorResponse,
    });
    const loader = createTextLoader('Loading next page', pageLoadingCls).addClass('h4');
    getPagination()
      .css('display', 'none')
      .after(loader);
  };

  private onNextPageLinkScrolledTo = () => {
    if (this.infiniteScrollState.loading) {
      return;
    }
    this.startLoadingNextPage();
  };

  private isValidPathForInfiniteScroll = (path: string) => true;

  private activateInfiniteScrollCoolDown = () => {
    debugLog('activateInfiniteScrollCoolDown called');
    if (this.infiniteScrollState.coolDownActive && this.infiniteScrollState.coolDownTimeoutId) {
      clearTimeout(this.infiniteScrollState.coolDownTimeoutId);
    }
    this.infiniteScrollState.coolDownActive = true;
    this.infiniteScrollState.coolDownTimeoutId = setTimeout(() => {
      debugLog('activateInfiniteScrollCoolDown - cool down ended');
      this.infiniteScrollState.coolDownActive = false;
      this.infiniteScrollState.coolDownTimeoutId = null;
      if (this.infiniteScrollState.loadNextPageAfterCoolDownFinishes) {
        this.startLoadingNextPage();
        this.infiniteScrollState.loadNextPageAfterCoolDownFinishes = false;
      }
    }, coolDownWhenCombinedWithFullHidingOfPosts) as unknown as number;
  };

  private checkScrollPosition = (cfg: Config) => () => {
    const doc = $(document);
    if (!this.isValidPathForInfiniteScroll(window.location.pathname)) {
      return;
    }
    const nextPageLink = getNextPageLink();
    if (!nextPageLink[0]) {
      return;
    }
    const offset = cfg.infiniteScroll.loadEarlier ? -2000 : 0;
    if ((doc.scrollTop() || 0) + ($(window).height() || 0) >= nextPageLink.position().top + offset) {
      this.onNextPageLinkScrolledTo();
      if (cfg.infiniteScroll.loadEarlier && !this.infiniteScrollState.coolDownActive) {
        this.activateInfiniteScrollCoolDown();
      }
    }
  };

  async setup(args: ModuleSetupArgs, cfg: Config) {
    if (!cfg?.infiniteScroll?.enabled) {
      return;
    }
    debugLog('setupInfiniteScroll')
    $(document).on('scroll', this.checkScrollPosition(cfg));
    $('#main-content-col').css('margin-bottom', '2em');
    setTimeout(this.checkScrollPosition(cfg), 100);
    setInterval(this.checkScrollPosition(cfg), coolDownWhenCombinedWithFullHidingOfPosts);
  };

  onSomePostsFullyHidden(cfg: Config) {
    this.activateInfiniteScrollCoolDown();
  }
}
