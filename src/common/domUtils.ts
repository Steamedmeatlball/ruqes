export const addStyle = (style: string | string[], id?: string) => {
  style = style instanceof Array ? style.join('\n') : style;
  const el = $('<style type="text/css">' + style + '</style>');
  if (id) { el.prop('id', id); }
  $('head').append(el);
}

export const addCustomScript = (script: string | string[], id?: string) => {
  script = script instanceof Array ? script.join('\n') : script;
  const el = $('<script>' + script + '</script>');
  if (id) { el.prop('id', id); }
  $('body').append(el);
}
