import microtip from 'microtip/microtip.css';

import {addStyle} from 'common/domUtils';
import logoSvg from 'assets/logo.svg';

export const clsPrefix = 'RuqES-by-enefi--';
export const genClsName = (x: string) => clsPrefix + x;
export const expandBoxCls = genClsName('box');
export const expandBoxCloserCls = genClsName('box-closer');
export const expandBoxOpenedCls = genClsName('box-opened');
export const boxEmptyCls = genClsName('box-empty');
export const expandoBtnCls = genClsName('expando-button');
export const expandoStylesCls = genClsName('expando-styles');
export const boxImgCls = genClsName('box-img');
export const boxGuildCls = genClsName('box-guild');
export const boxEmbedCls = genClsName('box-embed');
export const boxMetaEmbedCls = genClsName('box-meta-embed');
export const boxMetaEmbedUrlCls = genClsName('box-meta-embed-url');
export const boxPostTextCls = genClsName('box-post-text');
export const boxPostCommentsCls = genClsName('box-post-comments');
export const boxPostCommentsWithButtonCls = genClsName('box-post-comments-with-button');
export const pageLoadingCls = genClsName('page-loader');
export const postLoadingCls = genClsName('post-loader');
export const loadingDotsCls = genClsName('loading-dots');
export const loadingDotsDarkCls = genClsName('loading-dots-dark');
export const codeBlockCls = genClsName('code-block');
export const textLoaderCls = genClsName('text-loader');
export const errorTextCls = genClsName('error-text');
export const pageLoadingErrorCls = genClsName('page-loading-error');
export const ruqesMarkCls = genClsName('ruqes-mark');
export const creatorMarkCls = genClsName('creator-mark');
export const creatorNameCls = genClsName('creator-name');
export const settingsBackdropCls = genClsName('settings-backdrop');
export const settingsModalCls = genClsName('settings-modal');
export const settingsModalVisibleCls = genClsName('settings-modal-shown');
export const settingsCaptionCls = genClsName('settings-caption');
export const settingsLogoCls = genClsName('settings-logo');
export const settingsExpandoButtonActionCharacterCls = genClsName('settings-expando-action-char');
export const settingsMenuCls = genClsName('settings-menu');
export const settingsActiveTabButtonCls = genClsName('settings-active-tab-button');
export const settingsExpandoOrdersCls = genClsName('settings-expando-orders');
export const settingsPostRulesListCls = genClsName('settings-post-rules-list');
export const settingsPostRulesTemplatesListCls = genClsName('settings-post-rules-templates-list');
export const settingsPostRulesTemplatesListEmptyCls = genClsName('settings-post-rules-templates-list-empty');
export const settingsPostRuleListItemCls = genClsName('settings-post-rule-list-item');
export const settingsPostRuleListItemDisabledCls = genClsName('settings-post-rule-list-item-disabled');
export const settingsPostRuleListItemNameCls = genClsName('settings-post-rule-list-item-name');
export const settingsPostRuleNewFormCls = genClsName('settings-post-rule-new-form');
export const settingsPostRuleTemplateFormCls = genClsName('settings-post-rule-template-form');
export const settingsPostRuleBodyCls = genClsName('settings-post-rule-body');
export const settingsPostRulesNumberCls = genClsName('settings-post-rules-number');
export const formCls = genClsName('form');
export const blurAnimatedCls = genClsName('blur-anim');
export const blurCls = genClsName('blur');
export const voteAnimCls = genClsName('vote-anim');
export const voteAnimHandlerAppliedCls = genClsName('vote-anim-handler-applied');
export const bigVoteArrowsOnMobileCls = genClsName('big-vote-arrows-on-mobile');
export const semiHiddenPostCls = genClsName('semi-hidden-post');
export const sidebarButtonCls = genClsName('sidebar-button');
export const sidebarButtonIconCls = genClsName('sidebar-button-icon');
export const sidebarClosedCls = genClsName('sidebar-closed');
export const sidebarIndependentScrollCls = genClsName('sidebar-independent-scroll');
export const sidebarFavoritesCls = genClsName('sidebar-favorites');
export const sidebarFavoritesSmallImageCls = genClsName('sidebar-favorites-small-image');
export const sidebarSmallGuildImageCls = genClsName('sidebar-small-guild-image');
export const sidebarFavoritesEditButtonCls = genClsName('sidebar-favorites-edit-button');
export const sidebarFavoritesGuildBtnCls = genClsName('sidebar-favorites-guild-btn');
export const sidebarFavoritesEditingCls = genClsName('sidebar-favorites-editing');
export const sidebarFavoritesShowOnlyInEditingCls = genClsName('sidebar-favorites-show-only-in-editing');
export const sidebarFavoritesItemButtonCls = genClsName('sidebar-favorites-item-button');
export const sidebarFavoritesEditItemCls = genClsName('sidebar-favorites-edit-item');
export const sidebarFavoritesUpItemCls = genClsName('sidebar-favorites-up-item');
export const sidebarFavoritesDownItemCls = genClsName('sidebar-favorites-down-item');
export const sidebarFavoritesDeleteItemCls = genClsName('sidebar-favorites-delete-item');
export const sidebarFavoritesEmptyNoticeCls = genClsName('sidebar-favorites-empty-notice');
export const hideScrollbarCls = genClsName('hide-scrollbar');
export const btnPrimaryCls = genClsName('btn-primary');
export const btnDangerCls = genClsName('btn-danger');
export const loadTitleButtonCls = genClsName('post-load-title-button');
export const loadTitleButtonLoadingCls = genClsName('post-load-title-button-loading');
export const uploadImageToImgbbButtonCls = genClsName('post-upload-imgbb-button');
export const uploadImageToImgbbButtonHighlightedCls = genClsName('post-upload-imgbb-button-highlighted');
export const uploadImageToImgbbButtonLoadingCls = genClsName('post-upload-imgbb-button-loading');
export const uploadImageToImgbbButtonSmallCls = genClsName('post-upload-imgbb-button-small');
export const postPreviewCls = genClsName('post-preview');
export const postPreviewButtonCls = genClsName('post-preview-button');
export const blurNsfwThumbnailCls = genClsName('blur-nsfw-thumbnail');
export const blurThumbnailCls = genClsName('blur-thumbnail');
export const widthAutoCls = genClsName('width-auto');
export const infiniteScrollCoolDownMarkerCls = genClsName('inifinite-scroll-cooldown-marker');
export const improvedTableCls = genClsName('improved-table');
export const nonAggressiveCls = genClsName('non-aggressive');
export const disablePostActionsJumpingAboveBarOnHoverCls = genClsName('disable-post-actions-jumping-above-bar-on-hover');
export const resizableDirectImageWrapperCls = genClsName('resizable-direct-image-wrapper');
export const resizableDirectImageCls = genClsName('resizable-direct-image');
export const imageThumbnailTypeIconCls = genClsName('image-thumbnail-type-icon');
export const imageThumbnailTypeIconBackgroundCls = genClsName('image-thumbnail-type-icon-bg');
export const imageThumbnailTypeIconOnMobileCls = genClsName('image-thumbnail-type-icon-on-mobile');
export const imageThumbnailTypeIconFACls = genClsName('image-thumbnail-type-icon-fa');
export const closeButtonInImageDialogCls = genClsName('close-button-in-image-dialog');
export const postRulesAppliedCls = genClsName('post-rules-applied');
export const advancedModuleJavaScriptId = genClsName('advanced-module-java-script');
export const advancedModuleCssId = genClsName('advanced-module-css');
export const commentBiggerFoldButtonsCls = genClsName('comment-bigger-fold-buttons');
export const commentUpDownVotesAsTextProcessedCls = genClsName('comment-up-down-votes-as-text-processed');
export const commentPreviewCls = genClsName('comment-preview');
export const commentPreviewAppliedCls = genClsName('comment-preview-applied');
export const commentCollapseByRegexProcessedCls = genClsName('comment-collapse-by-regex-processed');
export const commentImageUploadProcessedCls = genClsName('comment-image-upload-processed');
export const upDownVotesAsTextUpCls = genClsName('up-down-votes-as-text-up');
export const UpDownVotesAsTextDownCls = genClsName('up-down-votes-as-text-down');
export const upDownVotesAsTextWrapperCls = genClsName('up-down-votes-as-text-wrapper');
export const postUpDownVotesAsTextProcessedCls = genClsName('post-up-down-votes-as-text-processed');
export const postDownloadButtonProcessedCls = genClsName('post-download-button-processed');
export const imageUploadInputCls = genClsName('image-upload-input');

export const savePostProcessedCls = genClsName('save-post-processed');
export const saveCommentProcessedCls = genClsName('save-comment-processed');
export const saveGuildImageCls = genClsName('save-guild-image');
export const saveEmptySavesButtonPlaceholderCls = genClsName('save-empty-saves-button-placeholder');
export const saveTabButtonPostsCls = genClsName('save-tab-button-posts');
export const saveTabButtonCommentsCls = genClsName('save-tab-button-comments');
export const savePostsContainerCls = genClsName('save-posts-container');
export const saveCommentsContainerCls = genClsName('save-comments-container');

export const loadingAppCls = genClsName('loading-app');
export const postShowThumbnailOnHoverProcessedCls = genClsName('post-show-thumbnail-on-hover-processed');
export const postShowThumbnailOnHoverImageCls = genClsName('post-show-thumbnail-on-hover-image');
export const postShowThumbnailOnHoverWrapperCls = genClsName('post-show-thumbnail-on-hover-wrapper');
export const postThumbnailModeProcessedCls = genClsName('post-thumbnail-mode-processed');
export const postThumbnailModeCls = genClsName('post-thumbnail-mode');
export const themeLight = genClsName('theme-light');
export const themeDark = genClsName('theme-dark');

export const setupStyles = (isDarkTheme: boolean) => {
  const primaryColor = 'rgba(128, 90, 213, 1)';
  const ruqesColor = '#800080';
  const ruqesDarkColor = '#400040';
  const ruqesLighterColor = '#C000C0';
  const ruqesLightColor = '#E070E0';
  const containerBorderColor = isDarkTheme ? '#303030' : '#E2E8F0';
  const containerBackgroundColor = isDarkTheme ? '#181818' : '#D2D8E0';
  const containerBackgroundColorSecondary = isDarkTheme ? '#232323' : '#c8ced6';
  const textColor = isDarkTheme ? 'white' : 'black';
  const revTextColor = isDarkTheme ? 'black' : 'white';
  const arrowVoteColor = isDarkTheme ? '#303030' : '#cbd5e0';
  const upvoteColor = '#805ad5';
  const downvoteColor = '#38b2ac';
  const anchorColorInDarkTheme = '#cfcfcf';
  const anchorColorInLightTheme = '#121213';
  const anchorColor = isDarkTheme ? anchorColorInDarkTheme : anchorColorInLightTheme;
  const commentCollapseColor = isDarkTheme ? '#aaa' : '#cbd5e0';
  const loadingTextShadowColor = '#555';
  addStyle(microtip);
  addStyle(`
.${widthAutoCls} { width: auto; }
  
.${expandoBtnCls} {
  margin: -0.4em 1em -0.4em 0;
  padding: 0;
  display: flex;
  align-items: center;
  justify-content: center;
}

.${expandoBtnCls}:last-child {
  margin: -0.4em 0 -0.4em 1em;
}

.${expandoBtnCls}--style-bigWide {
  width: 3.25em;
  height: 2.5em;
}
.${expandoBtnCls}--style-big {
  width: 2.5em;
  height: 2.5em;
}
.${expandoBtnCls}--style-medium {
  width: 2em;
  height: 2em;
  font-size: 90%;
}
.${expandoBtnCls}--style-small {
  width: 1.5em;
  height: auto;
  margin: -0.2em 0.5em 0 0;
  border: 0;
  background: none;
}

.${expandBoxCls} {
  display: none;
  margin-top: 0.5em;
  padding-left: calc(10px + 0.6em);
  padding-right: 0.6em;
  word-break: break-word;
  position: relative;
}

.${expandBoxOpenedCls} {
  display: flex;
  flex-direction: column;
}

.${expandBoxCloserCls} {
  position: absolute;
  top: 0;
  bottom: 0;
  left: 2px;
  padding-right: 15px;
  width: 2px;
  cursor: pointer;
}
.${expandBoxCloserCls}::before {
  position: absolute;
  height: 100%;
  content: "";
  background-color: ${commentCollapseColor};
  width: 3px;
}
.${expandBoxCloserCls}:hover::before {
  background-color: ${primaryColor};
}

img.${boxImgCls} {}

.stretched-link::after { display: none; }

.${boxEmbedCls} {}

.${boxPostTextCls} {
  padding: 5px;
}

.${boxPostTextCls} p:last-child {
  margin-bottom: 0;
}

.${boxPostCommentsCls} {
  padding: 5px 5px 5px 0;
}

.${boxPostCommentsCls} .comment:first-child {
  margin-top: 0.5em;
}
.${boxPostCommentsWithButtonCls} {
  padding: 5px 5px 5px 0;
}

.${boxGuildCls} {
  background-color: ${containerBorderColor};
  border-radius: 0.25rem;
  font-size: 135%;
  margin: 0.5em;
  transition: background-color 0.1s, box-shadow 0.1s; 
}
.${boxGuildCls}:hover {
  background-color: ${containerBackgroundColorSecondary};
  box-shadow: ${revTextColor} inset 0px 0px 2px;
}
.${boxGuildCls} img {
  border-radius: 50%;
  width: 2.5em;
}

.${boxMetaEmbedCls} {
  border: 1px solid ${containerBorderColor};
  background: ${containerBackgroundColor};
  border-radius: 0.5em;
}
.${boxMetaEmbedUrlCls} {
  background: ${containerBorderColor};
  margin: 0 -0.5em 0.25em -0.5em;
  border-radius: 0.25em 0.25em 0 0;
  padding: 2px 5px;
  opacity: 0.5;
  text-align: center;
  position: absolute;
  width: 100%;
}

@keyframes ruqesLoadingDots {
  0% {
    transform: translate(0, 0) scale(1);
    color: ${primaryColor}
  }
  25% {
    transform: translate(0, -0.2em) scale(1.33);
  }
  50% {
    transform: translate(0, 0) scale(1);
  }
  55% {
  }
  65% {
    color: ${textColor}
  }
}

.${loadingDotsCls} i {
  animation-name: ruqesLoadingDots;
  animation-duration: 2s;
  animation-delay: 0s;
  animation-iteration-count: infinite;
  animation-timing-function: ease-in-out;
  display: inline-block;
}

.${loadingDotsCls} i:nth-child(2) { animation-delay: 0.2s; }
.${loadingDotsCls} i:nth-child(3) { animation-delay: 0.4s; }

@keyframes ruqesLoadingDotsDark {
  0% {
    transform: translate(0, 0) scale(1);
    color: ${ruqesColor};
  }
  25% {
    transform: translate(0, -0.2em) scale(1.33);
  }
  50% {
    transform: translate(0, 0) scale(1);
  }
  55% {
  }
  65% {
    color: ${ruqesDarkColor};
  }
}

.${loadingDotsDarkCls} i {
  animation-name: ruqesLoadingDotsDark;
}

.${codeBlockCls} {
  border: 1px solid ${containerBorderColor};
  padding: 1em;
  font-size: 66%;
  background-color: lightgray;
}

.${textLoaderCls} {
  font-weight: bold;
}

.${pageLoadingCls} {
  margin: 1em 0 3em 0;
  text-align: center;
}

.${errorTextCls} {
  font-weight: bold;
  color: red;
}

.${pageLoadingErrorCls} {
  text-align: left;
}

.${ruqesMarkCls} svg {
  width: 1.2em;
  height: 1.2em;
}

.${creatorMarkCls} svg {
  width: 1.2em;
  height: 1.2em;
}

.${creatorNameCls} {
  transition: color 0.2s;
  color: ${isDarkTheme ? '#bf40bf' : '#800080'};
}
.${creatorNameCls}:hover {
  color: ${textColor};
}

.${settingsBackdropCls} {
  position: fixed;
  width: 100%;
  height: 100%;
  background-color: ${`${containerBackgroundColor}66`};
  z-index: 10000;
  top: 0;
}

.${settingsModalCls}::-webkit-scrollbar {
    height: 5px;
    width: 5px;
    background: ${containerBorderColor};
}
.${settingsModalCls}::-webkit-scrollbar-thumb {
    background: ${containerBackgroundColor};
}
.${settingsModalCls} {
  position: fixed;
  left: 50%;
  top: 100px;
  background-color: ${containerBorderColor};
  border: 2px solid ${containerBackgroundColor};
  border-radius: 5px;
  padding: 0.5em 1em 0.8em 1em;
  transform: translate(-50%, 0%) scale(0.8);
  min-width: 550px;
  max-width: 36em;
  overflow-y: auto;
  max-height: calc(100% - 110px);
  opacity: 0;
  transition: transform 0.2s, opacity 0.1s;
  scrollbar-color: ${containerBackgroundColor} ${containerBorderColor};
  scrollbar-width: thin;
  display: flex;
  flex-direction: column;
}
.${settingsModalVisibleCls} {
  transform: translate(-50%, 0%) scale(1);
  opacity: 1;
}
@media (max-width: 575.98px) {
  .${settingsModalCls} {
    width: 100%;
    min-height: 100%;
    min-width: auto;
    max-width: 100%;
    top: 0;
  }
}

.${formCls} input[type="checkbox"] {
  width: auto;
  height: auto;
  display: inline-block;
  margin-right: 0.3em;
}

.${formCls} input[type="radio"] {
  width: 1.2em;
  height: auto;
  display: inline-block;
}

.${formCls} label {
  margin-bottom: 0;
}

.${formCls} .form-group {
  margin-bottom: 0;
  display: flex;
  align-items: center;
}

.${settingsMenuCls} {
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
}

.${settingsActiveTabButtonCls} {
  border-color: ${ruqesColor} !important;
}

.${settingsExpandoOrdersCls} {
}
.${settingsExpandoOrdersCls} input {
  width: 5em;
}
.${settingsExpandoOrdersCls} label {
  width: 7em;
}

.${settingsPostRuleListItemCls} {}
.${settingsPostRuleListItemCls}:nth-child(even) {
  background-color: rgba(0, 0, 0, 0.1);
}
.${settingsPostRuleListItemDisabledCls} {
  opacity: 0.5;
}

.${settingsPostRuleListItemNameCls} { 
  width: calc(100% - 8em);
  cursor: pointer;
}
.${settingsPostRuleListItemNameCls}:hover {
  text-decoration: underline;
}

.${settingsPostRuleBodyCls} {
  min-height: 20em;
}

.${settingsPostRulesTemplatesListCls} {
  display: grid;
  grid-template-columns: repeat(2, 50%);
}
.${settingsPostRulesTemplatesListEmptyCls} {
  grid-column: span 2;
}

.${blurAnimatedCls} {
  filter: blur(0);
  transition: filter 0.5s ease-in-out;
}

.${blurCls} {
  filter: blur(2px);
}

.${settingsCaptionCls} {
  text-shadow: ${isDarkTheme ? 'black' : '#999'} 1px 0.5px 0px;
}

.${settingsLogoCls} {
  display: flex;
}
.${settingsLogoCls} svg {
  width: 2em;
  height: 2em;
  margin-right: 0.2em;
}

.${settingsExpandoButtonActionCharacterCls} input {
  width: 4em;
}
.${settingsExpandoButtonActionCharacterCls} label {
  width: 10em;
}

@keyframes ruqesVoteEffect {
  0% {
    transform: scale(1);
    opacity: 0;
  }
  1% {
    opacity: 0.8;
  }
  90% {
    transform: scale(3);
  }
  100% {
    transform: scale(3.1);
    opacity: 0;
  }
}

.${voteAnimCls} {
  animation-name: ruqesVoteEffect;
  animation-duration: 0.75s;
  animation-delay: 0s;
  animation-iteration-count: 1;
  animation-timing-function: ease-in-out;
  position: absolute !important;
  left: 0%;
  top: 20%;
}
.${voteAnimCls}.fa-arrow-alt-up {
  color: ${upvoteColor} !important;
}
.${voteAnimCls}.fa-arrow-alt-down {
  color: ${downvoteColor} !important;
}

.${bigVoteArrowsOnMobileCls} .${voteAnimCls} {
  top: 0%;
}

#frontpage .voting.${bigVoteArrowsOnMobileCls},
#search .voting.${bigVoteArrowsOnMobileCls},
#userpage .voting.${bigVoteArrowsOnMobileCls},
#guild .voting.${bigVoteArrowsOnMobileCls}
{
  margin-bottom: -1rem;
  margin-top: -0.4rem;
}

.${bigVoteArrowsOnMobileCls} .arrow-up::before,
.${bigVoteArrowsOnMobileCls} .arrow-down::before,
.${bigVoteArrowsOnMobileCls} .arrow-up:hover::before,
.${bigVoteArrowsOnMobileCls} .arrow-down:hover::before
{
  font-size: 1.5rem;
}

.${semiHiddenPostCls} {
  opacity: 0.2;
  transition: opacity 0.33s;
  max-height: 1.5em;
  background-color: ${containerBackgroundColor};
  overflow: hidden;
  padding-top: 0 !important;
}
.${semiHiddenPostCls}:hover {
  opacity: 1;
  max-height: none;
}

.${sidebarButtonCls} {
  paddingTop: 0;
  position: fixed;
}

.${sidebarButtonCls}.opened {
  position: relative;
}

.${sidebarButtonIconCls}::before {
    cursor: pointer;
    font-size: 1rem;
    color: #777;
    font-family: "Font Awesome 5 Pro";
    font-weight: 900;
    content: "";
}

.${sidebarButtonCls}.opened .${sidebarButtonIconCls}::before {
    cursor: pointer;
    font-size: 1rem;
    color: #777;
    font-family: "Font Awesome 5 Pro";
    font-weight: 900;
    content: "";
}

.${sidebarClosedCls} {
    max-width: 3em;
    padding: 0;
}
.${sidebarClosedCls} > :not(:first-child) {
    display: none !important;
}
.${sidebarClosedCls} > .${sidebarIndependentScrollCls} > :not(:first-child) {
    display: none !important;
}

.${sidebarIndependentScrollCls} {
  position: fixed;
  overflow-y: auto;
  overflow-x: hidden;
  height: calc(100vh - 49px);
  padding-right: 15px;
}

.${hideScrollbarCls} {
  scrollbar-width: none;
  -ms-overflow-style: none;
}
.${hideScrollbarCls}::-webkit-scrollbar {
  width: 0px;
}

.${sidebarFavoritesCls} {
}

.${sidebarFavoritesSmallImageCls} .profile-pic, .${sidebarSmallGuildImageCls} .profile-pic {
  width: 20px;
  height: 20px;
}
.${sidebarSmallGuildImageCls} .guild-recommendations-list .guild-recommendations-item {
  margin-top: 0.5rem;
}

.${sidebarFavoritesSmallImageCls} ul.guild-recommendations-list li {
  margin-top: 0.5rem;
}

.${sidebarFavoritesEditButtonCls} {
  cursor: pointer;
  margin-right: 0.4em;
}
.sidebar-section .${sidebarFavoritesEditButtonCls} i {
  font-size: 1rem;
}

.${sidebarFavoritesEditingCls} {}

.${sidebarFavoritesShowOnlyInEditingCls} {
  display: none;
}
.${sidebarFavoritesEditingCls} .${sidebarFavoritesShowOnlyInEditingCls} {
  display: inline;
} 

.${sidebarFavoritesEditingCls} li:hover {
  background-color: ${containerBackgroundColor};
}

.${sidebarFavoritesItemButtonCls} {
  cursor: pointer;
  margin-left: 0.25em;
}
.${sidebarFavoritesItemButtonCls}.disabled {
  pointer-events: none;
  cursor: default;
}
.${sidebarFavoritesItemButtonCls} i {
  color: ${textColor} !important;
}
.${sidebarFavoritesItemButtonCls}.disabled i {
  color: ${containerBorderColor} !important;
}
.${sidebarFavoritesItemButtonCls}:not(.disabled):hover i {
  color: ${primaryColor} !important;
}

.${sidebarFavoritesEmptyNoticeCls} {
  background-color: ${containerBackgroundColor};
  border-radius: 1em;
}
.sidebar-section .${sidebarFavoritesEmptyNoticeCls} > i {
  font-size: 200%;
}

.${sidebarFavoritesGuildBtnCls} {
  position: absolute;
  left: 0.25em;
  bottom: 0.25em;
  font-size: 125%;
  color: ${textColor};
  z-index: 0;
  padding: 0 0.5em 0 0.25em;
}
.${sidebarFavoritesGuildBtnCls}:hover:before {
  content: '';
  width: 100%;
  height: 100%;
  position: absolute;
  left: 0;
  background-color: ${revTextColor};
  opacity: 0.66;
  z-index: -1;
}

.${sidebarFavoritesGuildBtnCls} i {
  text-shadow: ${revTextColor} 1px 0 0, ${revTextColor} 0 1px 0, ${revTextColor} -1px 0 0, ${revTextColor} 0 -1px 0;
}

.${sidebarFavoritesGuildBtnCls} span {
  display: none;
}

.${sidebarFavoritesGuildBtnCls}:hover {
  text-decoration: none;
}

.${sidebarFavoritesGuildBtnCls}:hover span {
  display: inline;
}

.${btnPrimaryCls} {
    color: rgb(207, 207, 207) !important;
    background-color: #6133c9;
    border-color: #5c31bf;
}

.${btnPrimaryCls}:not(:disabled):not(.disabled):active, .${btnPrimaryCls}:not(:disabled):not(.disabled).active, .show>.${btnPrimaryCls}.dropdown-toggle {
    color: #fff !important;
    background-color: #6133c9;
    border-color: #5c31bf;
}

.${btnDangerCls} {
    color: rgb(207, 207, 207) !important;
    background-color: #e53e3e;
    border-color: #e53e3e;
}

@keyframes ruqesSpinning {
  0% {
    transform: scale(1) rotate(0deg);
  }
  30% {
    transform: scale(1.2) rotate(108deg);
  }
  60% {
    transform: scale(0.8) rotate(216deg);
  }
  100% {
    transform: scale(1) rotate(360deg);
  }
}

.${loadTitleButtonCls} {
  margin-top: -0.4em;
}
.${loadTitleButtonCls} svg {
  width: 1.2em;
  height: 1.2em;
  margin-left: -0.3em;
  margin-right: 0.2em;
}
.${loadTitleButtonCls}.${loadTitleButtonLoadingCls} svg {
  animation: 1s infinite linear ruqesSpinning;
}

.${uploadImageToImgbbButtonCls} {
  margin-top: -0.4em;
  border: 1px dashed ${ruqesColor};
}
.${uploadImageToImgbbButtonCls} svg {
  width: 1.2em;
  height: 1.2em;
  margin-left: -0.3em;
  margin-right: 0.2em;
}
.${uploadImageToImgbbButtonCls}.${uploadImageToImgbbButtonSmallCls} svg {
  width: 1.0em;
  height: 1.0em;
  margin-top: -0.2em;
}
.${uploadImageToImgbbButtonCls}.${uploadImageToImgbbButtonLoadingCls} svg {
  animation: 1s infinite linear ruqesSpinning;
}

.${loadTitleButtonCls} ~ .${uploadImageToImgbbButtonCls} {
  margin-left: 0.5em;
}

.${uploadImageToImgbbButtonHighlightedCls} {
  border-style: solid;
}

body.${blurNsfwThumbnailCls} .card.nsfw .post-img {
  filter: blur(7px) saturate(0.3);
}

.${blurThumbnailCls} .post-img {
  filter: blur(7px) saturate(0.3);
} 

.${infiniteScrollCoolDownMarkerCls} {
  font-size: 130%;
  margin-left: 1em;
}

.${improvedTableCls} td, .${improvedTableCls} th {
  border: 1px solid ${containerBorderColor};
  padding: 0.2em 0.5em;
}
.${improvedTableCls} th {
  background-color: ${containerBackgroundColorSecondary};
}

.${nonAggressiveCls} {
  opacity: 0.33;
  transition: opacity 0.2s;
}
.${nonAggressiveCls}:hover {
  opacity: 1;
}

.${disablePostActionsJumpingAboveBarOnHoverCls} .post-actions:hover,
.${disablePostActionsJumpingAboveBarOnHoverCls} .post-actions:focus {
    z-index: 3;
}

.${resizableDirectImageCls} {
  cursor: nwse-resize;
}

.${resizableDirectImageWrapperCls} {
  display: relative;
  z-index: 3;
}

.${imageThumbnailTypeIconCls} {
  position: absolute;
  right: 1px;
  top: 49px;
  width: 20px;
  height: 20px;
  text-align: center;
  border-radius: 5px;
  pointer-events: none;
  opacity: 0.9;
  color: white;
  text-shadow: black 1px 0 0, black 0 1px 0, black -1px 0 0, black 0 -1px 0;
  z-index: 2; /* otherwise broken in FF */
}
.${imageThumbnailTypeIconBackgroundCls} {
  background: #00000033;
}
.${imageThumbnailTypeIconOnMobileCls} {
  right: 16px;
  top: 46px;
}
.${imageThumbnailTypeIconFACls} {
  color: white;
}

.${postThumbnailModeCls}-huge .${imageThumbnailTypeIconCls} {
  top: 240px;
}
.${postThumbnailModeCls}-large .${imageThumbnailTypeIconCls} {
  top: 84px;
}
.${postThumbnailModeCls}-small .${imageThumbnailTypeIconCls} {
  top: 15px;
}
.${postThumbnailModeCls}-icon .${imageThumbnailTypeIconCls} {
  top: 1px;
}

.${postThumbnailModeCls}-huge .${imageThumbnailTypeIconOnMobileCls} {
  top: 248px;
}
.${postThumbnailModeCls}-large .${imageThumbnailTypeIconOnMobileCls} {
  top: 90px;
}
.${postThumbnailModeCls}-small .${imageThumbnailTypeIconOnMobileCls} {
  top: 21px;
}
.${postThumbnailModeCls}-icon .${imageThumbnailTypeIconOnMobileCls} {
  top: 8px;
}

.${postPreviewCls} {
  border: 1px solid ${containerBorderColor};
  padding: 1em;
  background-color: ${containerBackgroundColor};
}
.${postPreviewButtonCls} {
  color: ${anchorColor};
  text-decoration: none;
}

.${commentPreviewCls} {
  position: relative;
  border: 1px solid ${containerBorderColor};
  padding: 0.5em;
  background-color: ${containerBackgroundColor};
  z-index: 0;
}

:not(.collapsed) > .${commentPreviewCls} {
  display: none;
}

.${commentPreviewCls}:after {
  content: "Comment preview";
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  padding: 0.5em;
  background-image: url("data:image/svg+xml;base64,${btoa(logoSvg)}");
  background-size: auto 75%;
  background-repeat: no-repeat;
  background-position: center;
  opacity: 0.05;
  overflow: hidden;
  text-align: right;
  z-index: -1;
}

@media (max-width: 767.98px) {
  .${commentBiggerFoldButtonsCls} .comment .comment-collapse::before {
    font-size: 15px;
  }
  .${commentBiggerFoldButtonsCls} .comment.collapsed .comment-collapse:hover::before {}
}

.${commentUpDownVotesAsTextProcessedCls} {}
$.{upDownVotesAsTextWrapperCls} {}
.${upDownVotesAsTextUpCls} {
  color: ${upvoteColor};
}
.${UpDownVotesAsTextDownCls} {
  color: ${downvoteColor};
}
.${upDownVotesAsTextUpCls}, .${UpDownVotesAsTextDownCls} {}

.${saveEmptySavesButtonPlaceholderCls} a {
  color: ${textColor};
}

.${saveCommentsContainerCls} .comment:first-child {
  margin-top: 0.66em;
}

.${loadingAppCls} {
  position: fixed;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.5);
  top: 0;
  z-index: 2000;
  display: flex;
  align-items: center;
  justify-content: center;
  color: ${ruqesDarkColor};
  font-size: 500%;
  flex-direction: column;
  user-select: none;
}
.${loadingAppCls} > * {
  background: rgba(255, 255, 255, 0.66);
  border-radius: 10px;
}
.${loadingAppCls} > div {
  text-shadow: 1px 1px 0px ${loadingTextShadowColor}, 2px 1px 0px ${loadingTextShadowColor}, 3px 1px 0px ${loadingTextShadowColor};
}
.${loadingAppCls} a {
  margin: 0.5em;
  padding: 0 0.5em;
  color: gray;
  font-size: 40%;
}
.${loadingAppCls} a:hover {
  text-decoration: none;
}

.${postShowThumbnailOnHoverWrapperCls} {
  display: flex;
  align-items: center;
  justify-content: flex-start;
  position: fixed;
  top: 0;
  bottom: 0;
  right: 0;
  z-index: 5000;
  padding: 1em;
  pointer-events: none;
}

.${postShowThumbnailOnHoverImageCls} {
  max-width: 100%;
  max-height: 100%;
  border: 1px solid ${ruqesColor};
  padding: 1px;
  border-radius: 5px;
  background-color: white;
}

.${postThumbnailModeCls}-huge .post-img {
  width: 375px;
  height: 262px;
}
.${postThumbnailModeCls}-large .post-img {
  width: 150px;
  height: 105px;
}
.${postThumbnailModeCls}-small .post-img {
  width: 50px;
  height: 35px;
}
.${postThumbnailModeCls}-icon .post-img {
  width: 22px;
  height: 21px;
}
.${postThumbnailModeCls}-hide {
  display: none !important ;
}
`);
  $('body').addClass(isDarkTheme ? themeDark : themeLight);
};
