import {pageLoadingCls} from 'common/styles';
import {$c, $i} from 'common/common';

export const getPosts = () =>
  $('#posts > .card, .posts > .card, #thread .card').filter((_, rawEl) => $(rawEl).closest('#GIFs').length === 0);
export const getPagination = (from?: JQuery) => $('ul.pagination', from).parent();
export const getNextPageLink = () => getPagination().find('a:contains(Next)');
export const getPageLoadingMarker = () => $c(pageLoadingCls);
export const getLeftSidebarEl = () => $('#sidebar-left');
export const getMyGuildsInSidebarEl = () => getLeftSidebarEl().find('a[href="/browse"]').closest('.mb-4');
export const getRightSidebarEl = () => $('.sidebar:not(.sidebar-left)');
export const getPostTitleInPostCreation = () => $i('post-title');
export const getPostUrlInPostCreation = () => $i('post-URL');
export const getTopLevelContainersForBlurring = () => $('#main-content-row, #submitform');
export const getPostListContainer = () => $i('main-content-col');
export const getComments = () => $('.comment-body .user-info').parent();
export const getCommentReplyToPart = (commentEl: JQuery): JQuery | null => {
  const replyEl = commentEl.next();
  if (!replyEl.prop('id')?.startsWith('reply-to-')) { return null; }
  return replyEl;
};
export const getReplyToPost = (): JQuery | null => {
  const r = $('.comments-count ~ .comment-write');
  return r.length ? r : null;
};
export const getPostActionsList = (from?: JQuery) => $('.post-actions > ul', from);
export const getPostActionsListOnDesktop = (from?: JQuery) =>
  getPostActionsList(from).filter((_, rawEl) => !$(rawEl).parent().parent().hasClass('d-md-none'));
export const getPostActionsListOnMobile = (from?: JQuery) =>
  getPostActionsList(from).filter((_, rawEl) => $(rawEl).parent().parent().hasClass('d-md-none'));
export const getGuildActionsBar = () => $('#main-content-col .sticky.guild-border-top > .col > .d-flex');
export const getCommentActionsList = (from?: JQuery) => $('.comment-actions > ul', from);
