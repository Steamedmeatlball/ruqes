import {dropWhile, is, map, range, tryCatch} from 'ramda';
import {decodeHTML, encodeHTML} from 'entities';
import moment from 'moment';

import {
  loadingDotsCls,
  ruqesMarkCls,
  textLoaderCls,
  UpDownVotesAsTextDownCls,
  upDownVotesAsTextUpCls,
  upDownVotesAsTextWrapperCls,
} from 'common/styles';
import logoSvg from 'assets/logo.svg';
import {getGuildActionsBar} from './selectors';

export const logPrefix = '[RuqES]';
export const scriptInfo = GM.info.script;
export const scriptVersion = scriptInfo.version;

let debugEnabled = false;
export const isDebugEnabled = () => debugEnabled;
export const enabledDebug = () => debugEnabled = true;

export const debugLog = (...xs: any[]) => isDebugEnabled() && console.log(logPrefix, '[D]', ...xs);
export const printLog = (...xs: any[]) => console.log(logPrefix, ...xs);
export const printError = (...xs: any[]) => console.error(logPrefix, ...xs);
export const $c = (cls: string, parent?: JQuery) => $(`.${cls}`, parent);
export const $i = (cls: string, parent?: JQuery) => $(`#${cls}`, parent);
export const setClass = (el: JQuery, cls: string, value: boolean) => {
  if (value) {el.addClass(cls);} else {el.removeClass(cls);}
  return el;
};

export const ofType = <T>(x: T): T => x;

export const createLoadingDots = (cls = '') => {
  const container = $('<span>').addClass(loadingDotsCls).addClass(cls);
  range(0, 3).map(() => $('<i>.</i>')).forEach(x => container.append(x));
  return container;
};

export const createRuqesMark = () => $('<span>').html(logoSvg).addClass(ruqesMarkCls);

export const createTextLoader = (text: string, cls: string) =>
  $('<div>')
    .addClass(textLoaderCls)
    .addClass(cls)
    .append(createRuqesMark())
    .append($('<span>').text(text).css('margin-left', '0.33em'))
    .append(createLoadingDots());

export const createSmallCommentButton = (text: string): JQuery =>
  $('<label>')
    .addClass('btn btn-secondary format d-inline-block m-0 ml-1')
    .append(createRuqesMark().css('opacity', 0.5))
    .append($('<span>').text(text).addClass('font-weight-bolder text-uppercase'));

export const isDarkTheme = async (): Promise<boolean> => {
  return new Promise((resolve, reject) => {
    const check = (attempts: number) => {
      if (attempts <= 0) {
        reject('isDarkTheme: no more attempts');
      }
      const desk = $('#dark-switch');
      const mob = $('#dark-switch-mobile');
      if (!desk.length && !mob.length) {
        setTimeout(check, 50, attempts - 1);
      } else {
        resolve(desk.hasClass('fa-toggle-on') || mob.hasClass('fa-toggle-on'));
      }
    };
    check(10);
  });
};

export const xhr = (cfg: GM.Request) => GM.xmlHttpRequest(cfg);

export type ResponseType = 'text' | 'json' | 'blob' | 'arraybuffer' | 'document';

type XhrPConfig = Omit<GM.Request, 'onload' | 'onerror'> & { responseType?: ResponseType };
export const xhrP = <T = unknown>(cfg: XhrPConfig): Promise<GM.Response<T>> =>
  new Promise<GM.Response<T>>((resolve, reject) => {
    xhr({
      ...cfg,
      onload(response: GM.Response<T>) {
        if (!response || !(response.status >= 200 && response.status <= 300)) {
          reject(response);
        } else {
          resolve(response);
        }
      },
      onerror(response: GM.Response<unknown>) {
        reject(response);
      },
    });
  });

export const xhrPJson = <T = unknown>(cfg: Omit<XhrPConfig, 'data'> & { data: object }): Promise<GM.Response<T>> => {
  const newData = JSON.stringify(cfg.data);
  return xhrP({
    ...cfg,
    data: newData,
    headers: {'content-type': 'application/json'},
  });
};

export const preventDefaults = (evt: Event | JQuery.Event) => {
  evt.preventDefault();
  evt.stopPropagation();
};

export const dropWhileString = (pred: (x: string) => boolean, y: string): string => (dropWhile as any)(pred, y);

interface RuqqusWindow {
  upvote(event: MouseEvent): void;

  downvote(event: MouseEvent): void;
}

export const getRuqqusWindow = () => unsafeWindow as any as RuqqusWindow;

export const getWindowEl = (): JQuery => $(window as any);

export const setupVoting = (el: JQuery): void => {
  el.find('.upvote-button').each((_, rawEl) => {
    rawEl.addEventListener('click', getRuqqusWindow().upvote, false);
  });
  el.find('.downvote-button').each((_, rawEl) => {
    rawEl.addEventListener('click', getRuqqusWindow().downvote, false);
  });
};

export const isString = (x: unknown): x is string => typeof x === 'string';
export const isObject = (x: unknown): x is object => is(Object, x);
export const isArray = (x: unknown): x is unknown[] => is(Array, x);
export const isNumber = (x: unknown): x is number => is(Number, x);

export const isValidCssColor = (x: string): boolean =>
  /^\w+$|^#[a-f0-9]{3,6}$|^rgba?\(\d+,\s*\d+,\s*\d+(:?,\s*[0-9\.]+)?\)$/.test(x);

export const lastIndex = (xs: unknown[]): number => xs.length - 1;

export const showConfirmDialog = (message: string): Promise<boolean> => Promise.resolve(window.confirm(message));

export const genTextUpDownCounters = (upCount: number, downCount: number, upClasses?: string, downClasses?: string): JQuery => {
  const upEl = $('<span>').text('+' + upCount).addClass(upDownVotesAsTextUpCls).addClass(upClasses);
  const downEl = $('<span>').text('-' + downCount).addClass(UpDownVotesAsTextDownCls).addClass(downClasses);
  return $('<div>').addClass('d-inline-block ml-1').addClass(upDownVotesAsTextWrapperCls).append(' · ').append(upEl).append(' | ').append(downEl);
};

export const parseUpDownCounts = (x: string): [number, number] =>
  (x || '0 | 0').split(/ \| /).map(x => Number.parseInt(x, 10)).map(Math.abs) as [number, number]; // improve types?

export interface PostInfo {
  title: string;
  author: string | null;
  guild: string;
  id: string;
  nsfw: boolean;
  score: number;
  vote: 'up' | null | 'down',
  url: string;
  link: string | null;
  thumbnail: string | null;
  date: string;
  dateRelative: string;
  dateRaw: string;
  pinned: boolean;
  previewModal: boolean;
  textPost: boolean;
}

export interface CommentInfo {
  id: string | null;
  author: string | null;
  link: string | null;
  text: string | null;
  score: number | null;
}

const defaultTextThumbnail = '/assets/images/icons/default_thumb_text.png';
const defaultLinkThumbnail = '/assets/images/icons/default_thumb_link.png';

export const extractPostInfo = (el: JQuery): PostInfo => {
  const dateEl = el.find('.card-block > .post-meta > span[data-toggle="tooltip"]').first();
  const upvoted = el.find('.arrow-up.active').length > 0;
  const downvoted = el.find('.arrow-down.active').length > 0;
  const postId: string = el.prop('id').replace(/^post-/, '');
  const dateRaw = dateEl.prop('title');
  const date = moment(dateRaw, '%DD %MMMM %YYYY at %HH:%mm:%ss').format();
  let guild = el.find('.post-meta a').filter((_, el) => $(el).text() !== '').first().text().trim().replace(/^\+/, '');
  if (!guild) { guild = $i('guild-name-reference').text(); }
  const link = el.find('.card-header a').prop('href') ?? null;
  let thumbnail = el.find('.post-img').prop('src');
  if (!thumbnail) { thumbnail = link ? defaultLinkThumbnail : defaultTextThumbnail; }
  const url = el.find('.post-actions .fa-comment-dots').parent().prop('href');
  return {
    title: el.find('.post-title').text().trim(),
    author: el.find('.user-name').first().text(),
    guild,
    id: postId,
    nsfw: el.find('.text-danger:contains(nsfw)').length > 0,
    score: Number.parseInt(el.find('.score').last().text()) ?? 0,
    vote: upvoted ? 'up' : downvoted ? 'down' : null,
    url,
    link,
    date,
    dateRaw,
    dateRelative: dateEl.text(),
    pinned: el.find('i.fa-thumbtack').length > 0,
    thumbnail,
    previewModal: el.find('.card-header a[data-toggle="modal"]').length > 0,
    textPost: link === url,
  };
};

export const parseIntOrNull = (x: string): number | null => {
  const p = parseInt(x);
  return isNaN(p) ? null : p;
};

export const getCommentIdFromUrl = (x: string | null): string | null => {
  const regex = /^.*\/\+.*\/(\w+)(?:(?:\?.*)?)$/;
  const res = (x || '').match(regex) || [];
  return res[1] || null;
};

export const extractCommentInfo = (el: JQuery): CommentInfo => {
  const link = el.find('.fa-link').parent().prop('href') || null;
  return {
    id: getCommentIdFromUrl(link),
    author: el.find('.user-name ').text() || null,
    link,
    score: parseIntOrNull(el.find('.points .score').first().text()),
    text: el.find('.comment-text').html()?.trim() || null,
  };
};

export const isCommentDeletedByAuthor = (el: JQuery): boolean => el.find('.user-info').text()?.includes('[Deleted by author]');

export const removeLeadingSlash = (x: string) => x.replace(/^\//, '');

export const isOnExactPathGen = (path: string) => () => {
  const currentPath = window.location.pathname;
  return removeLeadingSlash(path) === removeLeadingSlash(currentPath);
};

export const isOnPathOrSubpageGen = (firstPathNamePart: string) => () => {
  const currentPath = window.location.pathname;
  const paths = currentPath.split('/').filter(x => x !== '');
  return [paths[0], paths[1]].includes(removeLeadingSlash(firstPathNamePart));
};

export const isOnPostDetail = isOnPathOrSubpageGen('post');
export const isOnPostEmbed = isOnPathOrSubpageGen('embed');

const guildInPathnameRegexGen = () => /^\/\+([\w]+)$/;

export const isOnGuildPage = (): boolean => guildInPathnameRegexGen().test(window.location.pathname);

export const getGuildNameFromPathname = (): string | null =>
  window.location.pathname.match(guildInPathnameRegexGen())?.[1] || null;

import {isValidSort, Sort} from './config';

export const getCurrentSort = (): Sort | null => {
  const urlParams = new URLSearchParams(window.location.search);
  const sortValue = urlParams.get('sort');
  debugLog('getCurrentSort', urlParams, sortValue);
  return isValidSort(sortValue) ? sortValue : null;
};

export const getGuildImageOnGuildPage = (): string | null => getGuildActionsBar().find('.profile-pic-35').prop('src') || null;

const hashPrefix = 'RuqES:';

export const isOnRuqESUrl = (path: string): boolean => {
  if (!isOnExactPathGen('')) { return false; }
  const hash = window.location.hash;
  return hash === `#${hashPrefix}${path}`;
};

export const genRuqESUrl = (path: string): string => `/#${hashPrefix}${path}`;

export const genJsAnchor = () => $('<a>').prop('href', 'javascript:void(0)');

export const genNavigateToRuqESUrl = (path: string) => () => {
  window.location.href = genRuqESUrl(path);
  window.location.reload();
};

export const createMainMenuButtonForDesktop = (text: string, url: string | null, cb: () => void | null, iconClasses: string) => {
  const icon = $('<i class="fa-width-rem text-left mr-3"></i>').addClass(iconClasses);
  return (url === null ? genJsAnchor() : $('<a>'))
    .prop('class', 'dropdown-item')
    .text(text)
    .prepend(icon)
    .on('click', cb)
    .prop('href', url || undefined)
    ;
};

export const createMainMenuButtonForMobile = (text: string, url: string | null, cb: () => void | null, iconClasses: string) => {
  const icon = $('<i class="fa-width-rem mr-3">').addClass(iconClasses);
  const link = (url === null ? genJsAnchor() : $('<a>'))
    .addClass('nav-link')
    .append(icon)
    .append(text)
    .on('click', cb)
    .click(() => $('.navbar-toggler').click())
    .prop('href', url || undefined)
  ;
  return $('<li class="nav-item">').append(link);
};

export const getImageUrlFromPostThumbnail = (postEl: JQuery): string | null => {
  const linkToImage = postEl.find('.post-img').parent();
  const href = linkToImage.attr('href');
  if (linkToImage.data('toggle') !== 'modal' || !href) { return null; }
  return href;
};

export const getDomain = (url: string): string | null => {
  const regex = /^https?:\/\/([\w\-.]+)(?:\/.*)$/;
  if (!url) { return null; }
  return (url.match(regex))?.[1] ?? null;
};

export type ToolTipPosition =
  'top'
  | 'top-left'
  | 'top-right'
  | 'bottom'
  | 'bottom-left'
  | 'bottom-right'
  | 'left'
  | 'right';
export type ToolTipSize = 'small' | 'medium' | 'large' | 'fit';

export const withTooltip = (text: string, html: string) => (position: ToolTipPosition = 'bottom', size: ToolTipSize = 'fit'): string =>
  `<span role="tooltip" aria-label="${text}" data-microtip-position="${position}" data-microtip-size="${size}">${html}</span>`;

export const tryParseJSON: (x: string) => unknown | null = tryCatch(y => JSON.parse(y), () => null);

export const getElSize = (x: JQuery, inner = false): [number, number] | null => {
  const w = inner ? $(x).innerWidth() : $(x).width();
  const h = inner ? $(x).innerHeight() : $(x).height();
  if (w === undefined || h === undefined) { return null; }
  return [w, h];
};

export const isElementInViewport = (el: JQuery): boolean => {
  if (!el || !el.length) {return false;}
  const rect = el[0].getBoundingClientRect();

  return (
    rect.top >= 0 &&
    rect.left >= 0 &&
    rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
    rect.right <= (window.innerWidth || document.documentElement.clientWidth)
  );
};

export const scrollIntoViewIfNeeded = (el: JQuery): void => {
  if (!isElementInViewport(el)) { el[0]?.scrollIntoView(); }
};

export const decodeHtmlEntities = (x: string): string => decodeHTML(x);

export const decodeHtmlEntitiesAggressively = (x: string): string => {
  const loop = (a: string, n: number): string => {
    if (n > 10) {return a;} //
    else {
      const dec = decodeHtmlEntities(a);
      return dec === a ? a : loop(dec, n + 1);
    }
  };
  return loop(x, 0);
};

export const encodeHtmlEntities = (x: string): string => encodeHTML(x);

export const encodeHtmlEntitiesDeeplyInStrings = <T>(x: T): T => {
  if (isArray(x) || isObject(x)) { return map(encodeHtmlEntitiesDeeplyInStrings, x); }
  if (isString(x)) { return encodeHtmlEntities(x) as unknown as T; }
  return x;
};
