import {includes, assoc, mergeDeepLeft, pipe} from 'ramda';

import {debugLog, enabledDebug, scriptVersion, ofType} from 'common/common';
import {DeepPartial} from '../types/DeepPartial';

const settingsConfigKey = 'config';

export const expandoButtonStyles = ['bigWide', 'big', 'medium', 'small'] as const;
export type ExpandoButtonStyle = typeof expandoButtonStyles[number];
export const expandoButtonStyleNames: Record<ExpandoButtonStyle, string> = {
  small: 'Small (icon)',
  medium: 'Medium',
  big: 'Big',
  bigWide: 'Big - extra wide',
};
export const defaultExpandoButtonStyle: ExpandoButtonStyle = 'big';
export const asExpandoButtonStyleOrDefault = (x: string): ExpandoButtonStyle =>
  includes(x, expandoButtonStyles) ? x as ExpandoButtonStyle : defaultExpandoButtonStyle;

export const imageThumbnailIconTypeStyles = ['emoji', 'fa'] as const;
export type ImageThumbnailIconTypeStyle = typeof imageThumbnailIconTypeStyles[number];
export const imageThumbnailIconTypeStyleNames: Record<ImageThumbnailIconTypeStyle, string> = {
  emoji: 'Emoji',
  fa: 'Font Awesome',
};
const defaultImageThumbnailIconTypeStyle: ImageThumbnailIconTypeStyle = 'fa';
export const asImageThumbnailIconTypeStyleOrDefault = (x: string): ImageThumbnailIconTypeStyle =>
  includes(x, imageThumbnailIconTypeStyles) ? x as ImageThumbnailIconTypeStyle : defaultImageThumbnailIconTypeStyle;

export interface PostRule {
  name: string;
  body: string;
  enabled: boolean;
}

export interface PostSave {
  title: string;
  author: string | null;
  guild: string;
  id: string;
  nsfw: boolean;
  url: string;
  link: string | null;
  thumbnail: string | null;
  date: string;
  dateRelative: string;
  dateRaw: string;
  previewModal: boolean;
  savedOn: string;
}

export interface CommentSave {
  id: string;
  author: string;
  link: string;
  text: string;
  savedOn: string;
}

export type OpenTarget = '_self' | '_blank';

export interface FavoriteItemBase {
  label: string | null;
  image: string | null;
  openTarget: OpenTarget;
}

export const sortValues = ['hot', 'top', 'new', 'disputed', 'activity'] as const;
export type Sort = typeof sortValues[number];
export const isValidSort = (x: string): x is Sort => (sortValues as unknown as string[]).includes(x);

export const thumbnailModeValues = ['vanilla', 'small', 'icon', 'hide', 'large', 'huge'] as const;
export type ThumbnailMode = typeof thumbnailModeValues[number];
export const isValidThumbnailMode = (x: string): x is ThumbnailMode => (thumbnailModeValues as unknown as string[]).includes(x);
export const defaultThumbnailMode: ThumbnailMode = 'vanilla';
export const asThumbnailModeOrDefault = (x): ThumbnailMode => {
  const y = String(x);
  return isValidThumbnailMode(y) ? y : defaultThumbnailMode;
};
export const thumbnailModeNames: Record<ThumbnailMode, string> = {
  vanilla: 'Vanilla (no change)',
  small: 'Small',
  icon: 'Icon',
  hide: 'Hide',
  large: 'Large',
  huge: 'Huge',
};

export interface FavoriteGuild extends FavoriteItemBase {
  tag: 'guild';
  guildName: string;
  sort: Sort;
}

export interface FavoriteLink extends FavoriteItemBase {
  tag: 'link';
  url: string;
}

export type FavoriteItem = FavoriteGuild | FavoriteLink;

export const downloadButtonModes = ['twoClick', 'downloadServer'] as const;
export type DownloadButtonMode = typeof downloadButtonModes[number];
export const isValidDownloadButtonMode = (x: string): x is DownloadButtonMode => (downloadButtonModes as unknown as string[]).includes(x);
const defaultDownloadButtonMode: DownloadButtonMode = 'twoClick';
export const asDownloadButtonModeOrDefault = (x): DownloadButtonMode => {
  const y = String(x);
  return isValidDownloadButtonMode(y) ? y : defaultDownloadButtonMode;
};

export type Config = {
  scriptVersion: string,
  debug: {
    enabled: boolean,
    autoOpenSettings: boolean,
    insertTestPostRule: boolean,
    dontHideAppLoadingOverlay: boolean,
  },
  expandoButton: {
    enabled: boolean,
    resize: boolean,
    style: ExpandoButtonStyle,
    alignRight: boolean,
    textClosed: string,
    textOpened: string,
    showComments: boolean,
    autoExpandComments: boolean,
    hideToggleCommentsButton: boolean,
    postTextOrder: number,
    embedOrder: number,
    commentsOrder: number,
    closerEnabled: boolean,
    clickOnPostToOpen: boolean,
    hide: boolean,
    autoOpenOnDetailOnUnsupportedVideos: boolean,
    autoOpenOnDetail: boolean,
    deferLoadingOfPost: boolean,
    autoOpen: boolean,
  },
  infiniteScroll: {
    enabled: boolean,
    loadEarlier: boolean,
  },
  voting: {
    bigVoteArrowsOnMobile: boolean,
    clickEffect: boolean,
  },
  post: {
    openInNewTab: boolean,
    hidePostsFromGuilds: string,
    fullyHidePostsFromGuilds: boolean,
    blurNsfwThumbnails: boolean,
    hideAlreadyJoinedGuildsInDiscovery: boolean,
    improvedTableStyles: boolean,
    lessAggressivePatronBadges: boolean,
    disablePostActionsJumpingAboveBarOnHover: boolean,
    imageThumbnailIconType: boolean,
    imageThumbnailIconTypeStyle: ImageThumbnailIconTypeStyle,
    imageThumbnailIconTypeBackground: boolean,
    closeButtonInImageDialog: boolean,
    rulesEngineEnabled: boolean,
    rules: PostRule[],
    upDownVotesAsText: boolean,
    downloadButton: boolean,
    downloadButtonMode: DownloadButtonMode,
    showThumbnailOnHover: boolean,
    thumbnailMode: ThumbnailMode
  },
  createPost: {
    loadTitleButton: boolean,
    markdownPreview: boolean,
    openUrlButton: boolean,
  },
  comment: {
    ctrlEnterToSend: boolean,
    biggerFoldButtons: boolean,
    upDownVotesAsText: boolean,
    preview: boolean,
    autoCollapseCommentsRegex: string,
    hideTipButton: boolean,
    imageUpload: boolean,
  },
  sidebar: {
    rightButton: boolean,
    autoHideRight: boolean,
    autoHideRightButNotOnGuildPage: boolean,
    independentScroll: boolean,
    favoritesEnabled: boolean,
    favorites: FavoriteItem[],
    favoritesSmallImages: boolean,
    smallGuildImages: boolean,
    savedContentLinkInFeeds: boolean,
    hideContentOfMyGuilds: boolean,
  },
  external: {
    imgbbKey: string,
    downloadServerAddress: string,
  },
  advanced: {
    enabled: boolean,
    js: string,
    css: string,
  },
  save: {
    enabled: boolean,
    posts: PostSave[],
    postSavingEnabled: boolean,
    commentSavingEnabled: boolean,
    comments: CommentSave[],
  },
  application: {
    disableLoadingOverlay: boolean,
  }
}

export type PartialConfig = DeepPartial<Config>;

export const defaultConfig: Config = Object.freeze(ofType<Config>({
  scriptVersion,
  debug: {
    enabled: false,
    autoOpenSettings: false,
    insertTestPostRule: false,
    dontHideAppLoadingOverlay: false,
  },
  expandoButton: {
    enabled: true,
    resize: true,
    style: defaultExpandoButtonStyle,
    alignRight: false,
    textClosed: '+',
    textOpened: '-',
    showComments: true,
    autoExpandComments: false,
    hideToggleCommentsButton: false,
    postTextOrder: 1,
    embedOrder: 2,
    commentsOrder: 3,
    closerEnabled: true,
    clickOnPostToOpen: false,
    hide: false,
    autoOpenOnDetail: false,
    autoOpenOnDetailOnUnsupportedVideos: true,
    deferLoadingOfPost: false,
    autoOpen: false,
  },
  infiniteScroll: {
    enabled: true,
    loadEarlier: false,
  },
  voting: {
    bigVoteArrowsOnMobile: true,
    clickEffect: true,
  },
  post: {
    openInNewTab: false,
    hidePostsFromGuilds: '',
    fullyHidePostsFromGuilds: false,
    blurNsfwThumbnails: false,
    hideAlreadyJoinedGuildsInDiscovery: true,
    improvedTableStyles: true,
    lessAggressivePatronBadges: false,
    disablePostActionsJumpingAboveBarOnHover: false,
    imageThumbnailIconType: true,
    imageThumbnailIconTypeStyle: defaultImageThumbnailIconTypeStyle,
    imageThumbnailIconTypeBackground: false,
    closeButtonInImageDialog: true,
    rulesEngineEnabled: false,
    rules: [],
    upDownVotesAsText: true,
    downloadButton: false,
    downloadButtonMode: 'twoClick',
    showThumbnailOnHover: false,
    thumbnailMode: 'vanilla',
  },
  createPost: {
    loadTitleButton: true,
    markdownPreview: true,
    openUrlButton: true,
  },
  comment: {
    ctrlEnterToSend: true,
    biggerFoldButtons: true,
    upDownVotesAsText: true,
    preview: true,
    autoCollapseCommentsRegex: '',
    hideTipButton: false,
    imageUpload: true,
  },
  sidebar: {
    rightButton: true,
    autoHideRight: false,
    autoHideRightButNotOnGuildPage: false,
    independentScroll: true,
    favoritesEnabled: true,
    favorites: [],
    favoritesSmallImages: false,
    smallGuildImages: false,
    savedContentLinkInFeeds: true,
    hideContentOfMyGuilds: false,
  },
  external: {
    imgbbKey: '',
    downloadServerAddress: 'http://localhost:4527',
  },
  advanced: {
    enabled: false,
    js: '',
    css: '',
  },
  save: {
    enabled: true,
    posts: [],
    commentSavingEnabled: true,
    postSavingEnabled: true,
    comments: [],
  },
  application: {
    disableLoadingOverlay: false,
  },
}));

const upgradeConfig = (cfg: PartialConfig): Config => {
  return pipe(
    mergeDeepLeft(cfg),
    assoc('scriptVersion', scriptVersion),
  )(defaultConfig) as Config;
};

interface ReadConfigArgs {
  forceUpgrade?: boolean;
}

export const readConfig = async ({forceUpgrade}: ReadConfigArgs = {}): Promise<Config> => {
  const rawCfg = await GM.getValue(settingsConfigKey, defaultConfig) as PartialConfig;
  let cfg: Config;
  if (rawCfg.scriptVersion !== scriptVersion || forceUpgrade) {
    console.log(`[RuqES] Upgrading config ${rawCfg.scriptVersion} -> ${scriptVersion}`);
    const upgradedConfig = upgradeConfig(rawCfg);
    cfg = upgradedConfig;
    await writeConfig(upgradedConfig);
  } else {
    cfg = rawCfg as Config;
  }
  debugLog('readConfig', cfg);
  if (cfg.debug.enabled) { enabledDebug(); }
  return cfg;
};

export const writeConfig = async (x: Config) =>
  await GM.setValue(settingsConfigKey, x as any); // value can be an object, types are incorrect
