import {loadingAppCls, loadingDotsDarkCls, setupStyles} from 'common/styles';
import {
  isDarkTheme,
  logPrefix,
  scriptInfo,
  scriptVersion,
  $c,
  genJsAnchor,
  createLoadingDots,
  debugLog,
} from 'common/common';
import {setupSettings} from 'common/settings';
import {handleFirstSetupOfModules, registerClassModule} from 'common/modules';
import {ExpandoButtonModule} from 'modules/expandoButton/ExpandoButtonModule';
import {VotingModule} from 'modules/VotingModule';
import {SidebarModule} from 'modules/sidebar/SidebarModule';
import {CommentModule} from 'modules/CommentModule';
import {CreatePostModule} from 'modules/createPost/CreatePostModule';
import {InfiniteScrollModule} from 'modules/InfiniteScrollModule';
import {PostsModule} from 'modules/post/PostsModule';
import {readConfig} from 'common/config';
import {setupDebugEventListeners} from 'common/events';
import {AdvancedModule} from 'modules/AdvancedModule';
import {SaveModule} from 'modules/save/SaveModule';
import {setupGlobalApi} from './common/RuqESApi';

const printInitMessage = () => {
  const bg = 'background-color: lightyellow;';
  const a = 'color: green; font-weight: bold; padding: 1em 0 1em 1em;' + bg;
  const b = 'color: black; padding: 1em 0; ' + bg;
  const c = 'color: purple; font-weight: bold; padding: 1em 1em 1em 0; ' + bg;
  const d = '';
  const info = GM.info;
  console.log(`%c${logPrefix} ${scriptVersion}%c created by %cenefi%c\n\n'${scriptInfo.name}' running via ${info.scriptHandler} ${info.version} on ${JSON.stringify((info as any).platform)}`, a, b, c, d);
};

const createLoadingAppOverlay = async () => {
  const cfg = await readConfig();
  if (cfg.application.disableLoadingOverlay) { return; }
  const body = $('body');
  const wrapper = $('<div>').addClass(loadingAppCls);
  const box = $('<div>').text('Loading').addClass('px-3').append(createLoadingDots(loadingDotsDarkCls));
  const skipBtn = genJsAnchor().text('don\'t wait ➝').on('click', removeLoadingOverlay);
  wrapper.append(box).append(skipBtn);
  body.append(wrapper);
};

const removeLoadingOverlay = () => {
  $c(loadingAppCls).remove();
};

const waitForDocumentToBeReady = async (): Promise<void> => {
  await readConfig(); // sets debugEnabled, so debugLog can print
  return new Promise((resolve, reject) => {
    const check = () => {
      debugLog('waitForDocumentToBeReady', 'check');
      const isReady = $('head').length && $('body').length;
      if (isReady) { resolve(); } //
      else { setTimeout(check, 1); }
    };
    check();
  });
};

const init = async () => {
  printInitMessage();
  await waitForDocumentToBeReady();
  setupStyles(await isDarkTheme());
  await createLoadingAppOverlay();
  [
    new InfiniteScrollModule(),
    new ExpandoButtonModule(),
    new VotingModule(),
    new PostsModule(),
    new CommentModule(),
    new SidebarModule(),
    new CreatePostModule(),
    new AdvancedModule(),
    new SaveModule(),
  ].forEach(registerClassModule);
};

const setupDebug = async () => {
  const cfg = await readConfig();
  if (cfg.debug.enabled) {
    setupDebugEventListeners();
  }
};
const work = async () => {
  const cfg = await readConfig();
  if (!cfg.debug.dontHideAppLoadingOverlay) { removeLoadingOverlay(); }
  await setupSettings();
  await handleFirstSetupOfModules();
  await setupDebug();
  await setupGlobalApi();
};

init().then();
$(work);
