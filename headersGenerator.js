const gen = dev => data => {
    return {
        name: 'RuqES - ruqqus.com',
        version: dev ? `[version]-build.[buildNo]` : `[version]`,
        namespace: 'enefi',
        match: [
            'https://ruqqus.com/*',
            'https://dev.ruqqus.com/*',
            'https://linode.ruqqus.com/*',
            'http://ruqqus.localhost:8000/*',
        ],
        grant: [
            'GM.xmlHttpRequest',
            'GM.info',
            'GM.getValue',
            'GM.setValue',
        ],
        author: 'enefi',
        description: '"Ruqqus Enhancement Suite" brings infinite scroll, expando button and more. Recommended to be used with Violentmonkey. For more details see https://ruqqus.com/post/ldx/what-is-ruqes',
        require: [
            'https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js',
        ],
        homepageURL: 'https://ruqqus.com/post/ldx/what-is-ruqes',
        supportURL: 'https://ruqqus.com/post/p04/bug-reports-and-other-issues',
        runAt: 'document-start',
    };
};

module.exports = gen;
